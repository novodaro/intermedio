package com.seratic.essa.dbIntermedio

import java.util.Date;

class DetallePedidoIntermedio {

	Long idCabeceraPedidoIntermedio
	String codCompania
	Short smiOrden
	String codArticulo
	Integer cantidad
	Float precioSugerido
	Float precioEntregado
	Float igv
	String codPedidoBk
	Boolean estadoEnvio
	Date fechaEnvio
	String almacen

	static constraints = {
		codPedidoBk nullable: true, blank:true
		fechaEnvio nullable:true, blank:true
	}
	
	static mapping = {
		datasource 'intermedio'
	}

}
