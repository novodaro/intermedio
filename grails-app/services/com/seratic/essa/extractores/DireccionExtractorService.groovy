package com.seratic.essa.extractores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

class DireccionExtractorService {

	static transactional = false
	def direccionSqlService
	def sessionFactory_intermedio

	def extraer() {
		def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()
		def listaDireccion = DireccionesEssa.findAll(){
			tpRegistro in ['Actualizado', 'Insertado']
			tablaOrigen == 'Agenda'
		}
		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		listaDireccion.eachWithIndex { direccion, index ->
			def direccionIntermedio = DireccionIntermedio.findByIdCliente(direccion.op)?: new DireccionIntermedio()
			direccionIntermedio.idCliente = direccion.op
			direccionIntermedio.fechaExtraccion = new Date()
			direccionIntermedio.estadoEnvio = false
			direccionIntermedio.direccion = direccion.direccion
			direccionIntermedio.falloEnvio = false

			if(direccionIntermedio.save(flush:true)){
				listaIdRegistrados.add(direccion.id)
				if(index.mod(100)==0) {
					cleanUpGorm()
				}
				//println direccionIntermedio.id
			}else{
				println 'Ocurrio un inconveniente al tratar de guardar en el intermedio la dirección: ' + direccion.op
				println direccionIntermedio.errors
			}

		}
		if(listaIdRegistrados)
			direccionSqlService.actualizaEstado(listaIdRegistrados)

		println "-----FINALIZA REGISTRO: "+new Date()
	}

	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
