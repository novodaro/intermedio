package com.seratic.essa.dbEssa

import java.util.Date;

class Inventario_Mov {

	Long op
	String tablaOrigen
	Long linea
	Long idMoneda
	Date fecha
	Date fechaAplicacion
	Long motivo
	Long tipoTc
	Long itemId
	String idItem
	Long idItemAnexo
	Float factor
	Long idNs
	Long idLote
	Float valor
	Float cantidad
	Float cantidadAjuste
	Boolean esEntrada
	String idAlmacen
	Long idAlmacenAnexo
	String idDestino
	Long idDestinoAnexo
	Float stock
	Float valor0
	Float gasto0
	Float ctoProm0
	Float valor1
	Float gasto1
	Float ctoProm1
	Float tc1
	String inf
	Date tpTelefonica
	String tpRegistro
	
    static constraints = {
    }
	
	static mapping = {
		datasource 'essa'
		table 'Inventario_Mov'
		version false
		order "asc"
		
		id column: 'Op', sqlType: 'numeric(18,0)'
		tablaOrigen column: 'TablaOrigen', sqlType: 'varchar(20)'
		linea column: 'Linea', sqlType: 'numeric(18,0)'
		idMoneda column: 'ID_Moneda', sqlType: 'numeric(18,0)'
		fecha column: 'Fecha', sqlType: 'datetime'
		fechaAplicacion column: 'FechaAplicacion', sqlType: 'datetime'
		motivo column: 'Motivo', sqlType: 'numeric(18,0)'
		tipoTc column: 'TipoTc', sqlType: 'numeric(18,0)'
		itemId column: 'Item_ID', sqlType: 'numeric(18,0)'
		idItem column: 'ID_Item', sqlType: 'varchar(20)'
		idItemAnexo column: 'ID_ItemAnexo', sqlType: 'numeric(18,0)'
		factor column: 'Factor', sqlType: 'money'
		idNs column: 'ID_NS', sqlType: 'numeric(18,0)'
		idLote column: 'ID_Lote', sqlType: 'numeric(18,0)'
		valor column: 'Valor', sqlType: 'money'
		cantidad column: 'Cantidad', sqlType: 'money'
		cantidadAjuste column: 'Cantidad_Ajuste', sqlType: 'money'
		esEntrada column: 'EsEntrada', sqlType: 'bit'
		idAlmacen column: 'ID_Almacen', sqlType: 'varchar(20)'
		idAlmacenAnexo column: 'ID_AlmacenAnexo', sqlType: 'numeric(18,0)'
		idDestino column: 'ID_Destino', sqlType: 'varchar(20)'
		idDestinoAnexo column: 'ID_DestinoAnexo', sqlType: 'numeric(18,0)'
		stock column: 'Stock', sqlType: 'money'
		valor0 column: 'Valor_0', sqlType: 'money'
		gasto0 column: 'Gasto_0', sqlType: 'money'
		ctoProm0 column: 'CtoProm_0', sqlType: 'money'
		valor1 column: 'Valor_1', sqlType: 'money'
		gasto1 column: 'Gasto_1', sqlType: 'money'
		ctoProm1 column: 'CtoProm_1', sqlType: 'money'
		tc1 column: 'TC_1', sqlType: 'money'
		inf column: 'Inf', sqlType: 'varchar(200)'
		tpTelefonica column: 'TP_Telefonica', sqlType: 'datetime'
		tpRegistro column: 'TP_Registro', sqlType: 'varchar(20)'
		
	}
}
