package com.seratic.essa.limpiadores

import grails.transaction.Transactional
import groovy.sql.Sql
import grails.util.Holders

@Transactional
class PedidoLimpiadorService {

	def sessionFactory_intermedio

	def limpiar() {
		
		def grailsApplication = Holders.getGrailsApplication()
		def maxDiasLimpiaPedido = grailsApplication.config.maxDiasLimpiaPedido
		
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		try{
			String query = "delete detalle_pedido_intermedio where id_cabecera_pedido_intermedio in (select id from cabecera_pedido_intermedio where datediff(day, fecha_registro, getdate()) > "+maxDiasLimpiaPedido+")"
			def results = sql.execute(query)
			
			String query2 = "delete cabecera_pedido_intermedio where datediff(day, fecha_registro, getdate()) > "+maxDiasLimpiaPedido
			def results2 = sql.execute(query2)

		}
		catch(Exception e){
			println 'Error en la eliminación de pedidos='+e.message
		}
	}
}
