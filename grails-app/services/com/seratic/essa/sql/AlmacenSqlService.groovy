package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class AlmacenSqlService {

	def sessionFactory_essa
	
    def sp_ListaAlmacen() {
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		def results = sql.rows("""
									Select Distinct Agenda.ID_Agenda As ID, Agenda.AgendaNombre As Nombre
									From Agenda
									Inner Join AgendaRel On Agenda.ID_Agenda = AgendaRel.ID_Agenda
									Where AgendaRel.Res = 10
									Order By Nombre
									""")
		session.clear()
		//println "resultado procedimiento: "+results
		return results
    }
}
