package com.seratic.essa.dbEssa

class TCambioEssa {

    Date fecha
	Float compra
	Float venta
	Float operativo
	Date tpTelefonica
	String tpRegistro

    static constraints = {
    }
	
	static mapping = {
		datasource 'essa'
		table 'TCambio'
		version false
		order "asc"
		
		id column: 'ID_Moneda'
		compra column: 'Compra', sqlType: 'money'
		venta column: 'Venta', sqlType: 'money'
		operativo column: 'Operativo', sqlType: 'money'
		tpTelefonica column: 'TP_Telefonica', sqlType: 'datetime'
		tpRegistro column: 'TP_Registro', sqlType: 'varchar(20)'
	}
}
