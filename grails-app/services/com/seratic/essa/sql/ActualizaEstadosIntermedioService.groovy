package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class ActualizaEstadosIntermedioService {

	def sessionFactory_intermedio
	
    def actualizaEstadoCliente(listaIdRegistrados) {
		 
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")
		try{
			String query = "UPDATE cliente_intermedio SET estado_envio=1, fecha_envio=GETDATE() WHERE codigo IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Cliente='+e.message
		}
	}
	
	def actualizaEstadoClienteFallido(listaIdRegistrados) {
		
	   def session = sessionFactory_intermedio.currentSession
	   def sql = new Sql(session.connection())
	   String stringListaIdRegistrados = listaIdRegistrados as String
	   stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")
	   try{
		   String query = "UPDATE cliente_intermedio SET estado_envio=1, fecha_envio=GETDATE(), fallo_envio = 1 WHERE codigo IN ('"+stringListaIdRegistrados+"')"
		   def results = sql.executeUpdate(query)

	   }
	   catch(Exception e){
		   println 'Error en el cambio de estado de Cliente='+e.message
	   }
   }
	
	def actualizaEstadoRuta(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE ruta_intermedio SET estado_envio=1, fecha_envio=GETDATE() WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Ruta='+e.message
		}
	}
	
	def actualizaEstadoRutaFallido(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE ruta_intermedio SET estado_envio=1, fecha_envio=GETDATE(), fallo_envio=1 WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Ruta='+e.message
		}
	}
	
	def actualizaEstadoProducto(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE producto_intermedio SET estado_envio=1, fecha_envio=GETDATE() WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Producto='+e.message
		}
	}
	
	def actualizaEstadoProductoFallido(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE producto_intermedio SET estado_envio=1, fecha_envio=GETDATE(), fallo_envio=1 WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Producto='+e.message
		}
	}
	
	def actualizaEstadoAlmacen(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE almacen_intermedio SET estado_envio=1, fecha_envio=GETDATE() WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Almacen='+e.message
		}
	}
	
	def actualizaEstadoAlmacenFallido(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE almacen_intermedio SET estado_envio=1, fecha_envio=GETDATE(), fallo_envio=1 WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Almacen='+e.message
		}
	}
	
	def actualizaEstadoVendedor(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE vendedor_intermedio SET estado_envio=1, fecha_envio=GETDATE() WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Vendedor='+e.message
		}
	}
	
	def actualizaEstadoVendedorFallido(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE vendedor_intermedio SET estado_envio=1, fecha_envio=GETDATE(), fallo_envio=1  WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Vendedor='+e.message
		}
	}
	
	def actualizaEstadoStock(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE stock_intermedio SET estado_envio=1, fecha_envio=GETDATE() WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Inventario_Mov='+e.message
		}
	}
	
	def actualizaEstadoStockFallido(listaIdRegistrados) {
		def session = sessionFactory_intermedio.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE stock_intermedio SET estado_envio=1, fecha_envio=GETDATE(), fallo_envio=1 WHERE id IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Inventario_Mov='+e.message
		}
	}
}
