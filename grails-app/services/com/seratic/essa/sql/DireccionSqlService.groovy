package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class DireccionSqlService {

    def sessionFactory_essa
	
    def actualizaEstado(listaIdRegistrados) {
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")
		
		try{
			String query = "UPDATE Direcciones SET TP_Registro='Enviado' WHERE ID IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Direccion='+e.message
		}
    }
}
