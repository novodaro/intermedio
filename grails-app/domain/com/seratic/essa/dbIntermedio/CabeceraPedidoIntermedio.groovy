package com.seratic.essa.dbIntermedio

import java.util.Date;

class CabeceraPedidoIntermedio {

	String codCompania
	String codVendedor
	String codCliente
	String codCondventa
	Character codMoneda
	Float tipoCambio
	String direccionEntrega
	String glosa
	Date fechaRegistro
	Boolean procesado
	String codPedidoBk
	String idPedidoGV
	String idPedidoEssa
	Boolean estadoEnvio
	Date fechaEnvio
	Boolean tieneErrorDetalle
	Boolean validoParaCalculoStockVirtual
	
    static constraints = {
		codCompania size: 1..2
		codVendedor size: 1..4
		codCliente size: 1..20
		codCondventa size: 1..2
		codMoneda size: 1..1
		direccionEntrega nullable: true, blank:true, size: 0..100
		glosa nullable:true, blank:true, size: 0..100
		fechaRegistro nullable:true, blank:true
		codPedidoBk nullable:true, blank:true, size: 0..50
		fechaEnvio nullable:true, blank:true
		idPedidoEssa nullable:true, blank:true
    }
	
	static mapping = {
		datasource 'intermedio'
	}
	
}
