package com.seratic.essa.extractores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

class VendedorExtractorService {

	static transactional = false
	def sessionFactory_intermedio

	def extraer() {

		def almacen = AlmacenIntermedio.first()
		def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()
		def listaVendedor = VendedorEssa.list()
		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		listaVendedor.eachWithIndex { vendedor, index ->
			def vendedorIntermedio = VendedorIntermedio.findByCodigo(vendedor.id)?:new VendedorIntermedio()
			vendedorIntermedio.nombre = vendedor.nomVendedor
			vendedorIntermedio.codigo = vendedor.id
			vendedorIntermedio.clave = vendedor.clave
			vendedorIntermedio.perfil = 3
			vendedorIntermedio.estado = (vendedor.estado=='A')?'x':''
			vendedorIntermedio.campania = 1
			vendedorIntermedio.codigoVendedor = vendedor.id
			vendedorIntermedio.codigoAlmacen = almacen.codigo
			vendedorIntermedio.fechaExtraccion = new Date()
			vendedorIntermedio.estadoEnvio = false
			vendedorIntermedio.falloEnvio = false

			if(vendedorIntermedio.save(flush:true)){
				listaIdRegistrados.add(vendedor.id)
				if(index.mod(100)==0) {
					cleanUpGorm()
				}
			}else{
				println 'Ocurrio un inconveniente al tratar de guardar en el intermedio el vendedor: ' + vendedor.id
				println vendedorIntermedio.errors
			}

		}

		println "-----FINALIZA REGISTRO: "+new Date()
	}

	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
