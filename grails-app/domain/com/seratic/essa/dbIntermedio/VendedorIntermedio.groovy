package com.seratic.essa.dbIntermedio

import java.util.Date;

class VendedorIntermedio {

	String nombre
	String codigo
	String clave
	String perfil
	String estado
	String campania
	String codigoVendedor
	String codigoAlmacen
	Date fechaExtraccion
	Date fechaEnvio
	Boolean estadoEnvio
	Boolean falloEnvio
	
    static constraints = {
		fechaEnvio nullable:true, blank:true
		perfil nullable:true, blank:true
	}
	
    static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaExtraccion"
	}
}
