package com.seratic.essa.extractores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*
import com.seratic.essa.utils.*

class AgendaLimiteCreditoExtractorService {

	static transactional = false
	def agendaLimiteCreditoSqlService
	def sessionFactory_intermedio

	def extraer() {
		def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()

		def listaLimiteCredito = agendaLimiteCreditoSqlService.consultaAgendasDesactualzadas()

		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		listaLimiteCredito.eachWithIndex { agendaLimCredito, index ->
			def clienteIntermedio = ClienteIntermedio.findByCodigo(agendaLimCredito.idAgenda)
			if(clienteIntermedio){
				clienteIntermedio.estadoEnvio = false
				clienteIntermedio.falloEnvio = false

				if(clienteIntermedio.save(flush:true)){
					def listaId = Utilidad.clobToString(agendaLimCredito.listaAgenLimCredito)

					listaIdRegistrados.addAll(listaId)
					if(index.mod(100)==0) {
						cleanUpGorm()
					}
				}else{
					println 'Ocurrio un inconveniente al actualizar en el intermedio el cliente: ' + agendaLimCredito.idAgenda
					println clienteIntermedio.errors
				}
			}else{
				println 'No se ha podido actualizar el siguiente ClienteIntermedio debido a que no existe: '+agendaLimCredito.idAgenda
			}
		}
		if(listaIdRegistrados)
			agendaLimiteCreditoSqlService.actualizaEstado(listaIdRegistrados)
		println "-----FINALIZA REGISTRO: "+new Date()
	}

	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
