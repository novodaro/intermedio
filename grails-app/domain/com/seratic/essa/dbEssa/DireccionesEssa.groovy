package com.seratic.essa.dbEssa

class DireccionesEssa {

    String tablaOrigen
	String op
	Integer tipoDireccion
	Long tipoCalle
	String direccion
	String numero
	String departamento
	Long idDistrito
	Date tpTelefonica
	String tpRegistro

	static constraints = {
	}

	static mapping = {
		datasource 'essa'
		table 'Direcciones'
		version false
		order "asc"

		id column: 'ID', sqlType: 'numeric(18,0)'
		tablaOrigen column: 'TablaOrigen'
		op column: 'Op', sqlType: 'varchar(20)'
		tipoDireccion column: 'TipoDireccion'
		tipoCalle column: 'TipoCalle'
		direccion column: 'Direccion'
		numero column: 'Numero'
		departamento column: 'Departamento'
		idDistrito column: 'ID_Distrito'
		tpTelefonica column: 'TP_Telefonica'
		tpRegistro column: 'TP_Registro'
	}
}
