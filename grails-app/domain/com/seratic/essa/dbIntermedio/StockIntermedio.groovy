package com.seratic.essa.dbIntermedio

import java.util.Date;

class StockIntermedio {

	String codigoProducto
	String codigoAlmacen
	Long stock
	Long precio
	Long moneda 
	Date fechaExtraccion
	Date fechaEnvio
	Date fechaModificacionEssa
	Boolean estadoEnvio
	Long stockVirtual
	String unidadProducto
	Boolean falloEnvio
	
    static constraints = {
		fechaEnvio nullable:true, blank:true
		stockVirtual nullable:true, blank:true
    }
	
	static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaModificacionEssa"
	}
}
