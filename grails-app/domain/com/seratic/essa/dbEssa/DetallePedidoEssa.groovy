package com.seratic.essa.dbEssa

class DetallePedidoEssa implements Serializable{

	Long codPedido
	String codCompania
	Short smiOrden
	String codArticulo
	Integer cantidad
	Float precioSugerido
	Float precioEntregado
	Float igv
	String codPedidoBk
	
    static constraints = {
		codPedidoBk nullable: true, blank:true
    }
	
	static mapping = {
		datasource 'essa'
		table 'DETALLE_PEDIDO'
		version false
		order "asc"
		
		id composite: ['codPedido', 'codCompania', 'smiOrden']
		codPedido   column:'vc_codpedido'
		codCompania column:'vc_codcompania'
		smiOrden column:'smi_orden'
		codArticulo column:'vc_codarticulo'
		cantidad column:'int_cantidad'
		precioSugerido column:'flt_preciosugerido'
		precioEntregado column:'flt_precioentregado'
		igv column:'flt_igv'
		codPedidoBk column:'vc_codpedido_bk'
	}
	
	
}
