package com.seratic.essa.utils

import java.sql.Clob;
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat

class Utilidad {

	public static String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while(null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (Exception e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();
	}
}
