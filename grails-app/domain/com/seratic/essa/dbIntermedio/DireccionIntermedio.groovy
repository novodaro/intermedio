package com.seratic.essa.dbIntermedio

import java.util.Date;

class DireccionIntermedio {

	String idCliente
	Date fechaExtraccion
	Date fechaEnvio
	Boolean estadoEnvio
	String direccion
	Boolean falloEnvio
	
	static constraints = {
		fechaEnvio nullable:true, blank:true
	}

	static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaExtraccion"
	}
}
