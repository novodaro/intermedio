package com.seratic.essa.descargadores

import grails.transaction.Transactional
import grails.util.Holders
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import wslite.rest.RESTClient
import com.seratic.essa.dbIntermedio.*

@Transactional
class PedidoDescargadorService {

	def descargar() {

		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase
		def igv = grailsApplication.config.igv

		def formatoDecimal = new DecimalFormat("0.0000")

		SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		def fechaActual = formatoFecha.format(new Date()).replace(' ', 'T')
		def variableFecha = VariableIntermedio.findByNombre('ultimaDescargaPedidos')

		def fechaAnterior = variableFecha?variableFecha.valor:fechaActual

		def fechaActual2 = fechaActual.replaceAll('/', '-')
		def fechaAnterior2 = fechaAnterior.replaceAll('/', '-')

		def listaProductosPedidos = []

		try{

			def client = new RESTClient(urlBase)
			def respuesta = client.get(path:'/GestorVisitas/restApi/form.php', query:[codCuenta:1 , codCampana:1, codActividad:1, fechaInicio:fechaAnterior, fechaFin: fechaActual, idUsuarioInvoca:1])
			def listaPedidos = respuesta.json
			
			def listaPedidosRegistrados = []
			
			println listaPedidos.Detalle

			listaPedidos.each{pedidoJson->
				def cabeceraPedidoIntermedio = CabeceraPedidoIntermedio.findByIdPedidoGV(pedidoJson.idPedido)
				def clienteIntermedio = ClienteIntermedio.findByNombre(pedidoJson.pv_Cliente)
				def vendedorntermedio = VendedorIntermedio.findByCodigo(pedidoJson.u_CODIGOVENDEDOR)

				if(!cabeceraPedidoIntermedio && clienteIntermedio && !vendedorntermedio.equals(null)){
					cabeceraPedidoIntermedio = new CabeceraPedidoIntermedio()
					cabeceraPedidoIntermedio.codCompania = "01" //TODO se enviará siempre 01
					cabeceraPedidoIntermedio.codVendedor = vendedorntermedio.codigo
					cabeceraPedidoIntermedio.codCliente = clienteIntermedio.codigo
					cabeceraPedidoIntermedio.codCondventa = pedidoJson.codIntTipodePago // actualmente esta llegando vacio
					cabeceraPedidoIntermedio.codMoneda =  pedidoJson.ListadePrecio=='1'?'S':'D' //2018-11-14, Se solicita este ajuste por Josue
					cabeceraPedidoIntermedio.tipoCambio = 0 //TODO de donde se sacará esta información?
					cabeceraPedidoIntermedio.glosa = "Gestor Visitas"
					cabeceraPedidoIntermedio.fechaRegistro = Date.parse("yyyy/MM/dd HH:mm:ss", pedidoJson.FechayHora)
					cabeceraPedidoIntermedio.procesado = 0
					cabeceraPedidoIntermedio.estadoEnvio = false
					cabeceraPedidoIntermedio.tieneErrorDetalle = false
					cabeceraPedidoIntermedio.idPedidoGV = pedidoJson.idPedido
					cabeceraPedidoIntermedio.validoParaCalculoStockVirtual = true

					if(cabeceraPedidoIntermedio.save(flush:true)){
						try {
							pedidoJson.Detalle.eachWithIndex{ detallePedidoJson, index ->
								//VALIDAR COMO SE MANEJA EL STOCK VIRTUAL, TENIENDO EN CUENTA EL ALMACEN
								println 'error'
								println detallePedidoJson
								def productoIntermedio = ProductoIntermedio.findByCodigo(detallePedidoJson.codigo)
								def detallePedidoIntermedio = new DetallePedidoIntermedio()
								detallePedidoIntermedio.codCompania = "01"
								detallePedidoIntermedio.idCabeceraPedidoIntermedio = cabeceraPedidoIntermedio.id
								detallePedidoIntermedio.smiOrden = index+1
								detallePedidoIntermedio.codArticulo = productoIntermedio.codigo
								detallePedidoIntermedio.cantidad = detallePedidoJson.cantidad as Integer
								detallePedidoIntermedio.precioSugerido = productoIntermedio.precio as Float//TODO Averiguar que se enviará en los precios
								detallePedidoIntermedio.precioEntregado = detallePedidoJson.precio as Float
								detallePedidoIntermedio.igv = igv as Float
								detallePedidoIntermedio.estadoEnvio = false
								detallePedidoIntermedio.almacen = pedidoJson.Almacen
								if(!detallePedidoIntermedio.save(flush:true)){
									println '1. Se presento inconveniente en un registro del Detalle Pedido de la Cabecera Pedido: '+ cabeceraPedidoIntermedio.id
									cabeceraPedidoIntermedio.tieneErrorDetalle = true
									cabeceraPedidoIntermedio.save(flush:true)
								}
							}
						}catch(Error | Exception  e){
							println new Date()
							println '2. Se presento inconveniente en un registro del Detalle Pedido de la Cabecera Pedido: '+ cabeceraPedidoIntermedio.id
							cabeceraPedidoIntermedio.tieneErrorDetalle = true
							cabeceraPedidoIntermedio.save()
							println "Error presentado en el Detalle de Pedidos "+ e
						}
						def fechaUltimaDescargaPedidos = variableFecha?:(new VariableIntermedio(nombre: 'ultimaDescargaPedidos'))
						fechaUltimaDescargaPedidos.valor= fechaActual
						fechaUltimaDescargaPedidos.save(flush:true)
					}else {
						println new Date()
						println "Error al almacenar el pedido: "+ pedidoJson.idPedido + " error: "+ cabeceraPedidoIntermedio.errors
					}

				}else{
					println "El pedido ya existe o no contiene información valida para ser almacenado: "+ pedidoJson.idPedido
					println pedidoJson
				}


			}

			return listaPedidosRegistrados


		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Pedidos "+ e
			return null
		}


	}
}
