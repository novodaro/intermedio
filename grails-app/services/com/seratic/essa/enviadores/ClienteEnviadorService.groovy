package com.seratic.essa.enviadores

import grails.transaction.Transactional
import wslite.soap.SOAPClient
import com.seratic.essa.dbIntermedio.*
import grails.util.Holders
import com.seratic.essa.utils.*


class ClienteEnviadorService {
	static transactional = false

	def agendaSqlService
	def actualizaEstadosIntermedioService

	def enviar(listaClienteIntermedio) {

		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase

		def respuesta = [statusCode:200, message:'', data: []]

		def listaEnviados =[]
		def listaErrores = []

		try{
			def listaClienteString = ""


			println "---Carga Puntos Venta---"
			listaClienteIntermedio.eachWithIndex { clienteIntermedio, index ->

				try {
					def cuentaCorriente = agendaSqlService.transformaAFormatoGVListas(agendaSqlService.cuentaCorrienta(clienteIntermedio.codigo))
					def lineaCredito = agendaSqlService.transformaAFormatoGVListas(agendaSqlService.lineaCredito(clienteIntermedio.codigo, clienteIntermedio.moneda, clienteIntermedio.fechaIngreso))

					def puntoVentaString = """
					<puntosVenta>
				              
				               <accion>MA</accion> <!--M:modifica; A:adiciona-->
				              
				               <columnas><![CDATA[${clienteIntermedio.codigo}]]></columnas><!--Código varchar100 vc_codcliente-->
				               <columnas><![CDATA[${clienteIntermedio.nombre}]]></columnas><!--nombre cliete varchar 100 -->  
				               <columnas><![CDATA[${clienteIntermedio.tipoDocumento}]]></columnas><!--Tipo documento DNI o RUC--> 
				               <columnas><![CDATA[${clienteIntermedio.direccion?:'-'}]]></columnas><!--dirección: varchar 100--> 
				               <columnas><![CDATA[${clienteIntermedio.numeroDocumento}]]></columnas><!--dni/RUC: numerico-->  
				               <columnas><![CDATA[${cuentaCorriente}]]></columnas><!--* Cuenta corriente: varchar(3000)--> 
				               <columnas><![CDATA[${lineaCredito}]]></columnas><!--* Línea de crédito: varchar(3000)--> 
							   <columnas>0 0</columnas><!--ubicación (0 0) lat long-->
				               
				               <usuario>Administrador</usuario>
				   </puntosVenta>
			"""


					listaEnviados.add(clienteIntermedio.codigo)
					listaClienteString = listaClienteString+ puntoVentaString

				}catch(Exception| Error e){
					println 'Error presentado en el servicio de Cliente Enviador: '+e.message
					listaErrores.add(clienteIntermedio.codigo)
				}
			}



			def client = new SOAPClient(urlBase+'/GestorVisitas/WSCargaMaestros?wsdl')
			def response = client.send(
					"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://com.ws.gestorvisitas.enterprise.seratic.org/">
				   <soapenv:Header/>
				   <soapenv:Body>
				      <com:wmSincronizarPuntosVenta>
				         <!--Optional:-->
				         <puntosVentaRQT>
				            <!--Zero or more repetitions:-->
				            """+listaClienteString+"""
									 </puntosVentaRQT>
								  </com:wmSincronizarPuntosVenta>
							   </soapenv:Body>
							</soapenv:Envelope>"""
					)


			respuesta.statusCode = response.httpResponse.statusCode

			response.wmSincronizarPuntosVentaResponse.return.resultados.errores.fila.each{e->
				listaErrores.add(listaEnviados[e.text().toInteger()-1])
			}

			respuesta.message=listaErrores.empty?"Todos los registros fueron actualizados":"Existieron registros que no se almacenaron correctamente"
			respuesta.data = listaErrores.unique()

			if(!listaErrores.empty){
				println new Date()
				println listaErrores
				//println listaClienteString
				println response.text
			}

		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Punto Ventas "+ e
			respuesta.statusCode = 500
			respuesta.message = e.message
			
			listaErrores = listaEnviados

		}


		def diferenciaId = listaEnviados - listaErrores

		if(diferenciaId)
			actualizaEstadosIntermedioService.actualizaEstadoCliente(diferenciaId)

		if(listaErrores)
			actualizaEstadosIntermedioService.actualizaEstadoClienteFallido(listaErrores)

		println respuesta
		return respuesta

	}


	def cargaMultiple(max){

		def respuesta = [:]

		def listaRegistrosNoActualizados = []

		def status = 200
		def listaIdRegistros = []

		def totalRegistros = 0

		while(true){

			def listaRegistros =   ClienteIntermedio.findAll([max: max]){
				estadoEnvio == false
				falloEnvio == false
			}

			listaIdRegistros.addAll(listaRegistros.id)

			def respuestaTemporal = enviar(listaRegistros)

			listaRegistrosNoActualizados.add(respuestaTemporal.data)
			status = respuestaTemporal.statusCode!=200?respuestaTemporal.statusCode:status

			if(!listaRegistros)break
		}

		listaRegistrosNoActualizados = listaRegistrosNoActualizados.flatten()
		totalRegistros = listaIdRegistros.size()

		def registrosActualizadosTotal = totalRegistros-listaRegistrosNoActualizados.size()

		respuesta.status = status
		respuesta.message = 'Se registraron '+registrosActualizadosTotal+' de '+totalRegistros
		respuesta.data = listaRegistrosNoActualizados

		return respuesta
	}

}
