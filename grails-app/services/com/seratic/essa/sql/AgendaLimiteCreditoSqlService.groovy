package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class AgendaLimiteCreditoSqlService {
	
	def sessionFactory_essa

    def actualizaEstado(listaIdRegistrados) {
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replaceAll("\\s","").replace('[','').replace(']', '').replace(",","','")
		
		try{
			String query = "UPDATE AgendaLimiteCredito SET TP_Registro='Enviado' WHERE ID IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de AgendaLimiteCredito='+e.message
		}
    }
	
	def consultaAgendasDesactualzadas(){
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		def results = sql.rows("""select
										STUFF((
										       SELECT ',' + CAST(alc2.ID AS VARCHAR)
										         FROM AgendaLimiteCredito alc2
										        WHERE alc2.ID_Agenda = alc.ID_Agenda
										          FOR XML PATH('')), 1, LEN(','), '') as listaAgenLimCredito,
												   alc.ID_Agenda as idAgenda 
										from  AgendaLimiteCredito alc
										where alc.TP_Registro in ('Insertado', 'Actualizado')
										group by ID_Agenda
									""")
		session.clear()
		//println "resultado procedimiento: "+results
		return results
	}
}
