package com.seratic.essa.dbEssa

class AgendaEssa {

    String id
	Integer tipoPersona
	Long estado
	String agendaNombre
	String razonSocial
	String nombre
	String nombre2
	String apellidoPaterno
	String apellidoMaterno
	String ruc
	String webPage
	String correoElectronico
	String contacto
	Long idMonedaVta
	Long idMonedaCpra
	Float tea
	Integer diasCredito
	String idVendedor
	Long tipoVendedor
	Long idafp
	Long idMonedaRemuneracion
	Long idCargo
	Long idSexo
	Long idGradoInstruccion
	Long idProfesion
	Long idEstadoCivil
	String codigoAFP
	Date fechaIngreso
	Float remuneracion
	String conyuge
	Integer nroHijos
	String codigoEmpleado
	Long idArea
	Boolean afectoPagoQuincena
	Long idDescuento
	byte[] foto
	String tarjetaNumero
	String tratamiento
	Long idMedioContacto
	String diaVisita
	String nroCelularEmpresa
	Long limiteCreditoIdMoneda
	Float limiteCreditoMonto
	String ruta
	Boolean asignacionFamiliar
	Boolean ipssVida
	Boolean aportePacifico
	Date fechaNacimiento
	Date fechaActualizacion
	String aux1
	String nombreComercial
	Long idClas1
	Long idClas2
	Long idClas3
	String observaciones
	Boolean generaPuntos
	Long idZona
	Boolean esBuenContribuyente
	Boolean esAgenteRetencion
	String ubicacion
	Date tpTelefonica
	String tpRegistro
	
	
    static constraints = {
    }
	
	static mapping = {
		datasource 'essa'
		table 'Agenda'
		version false
		order "asc"
		
		
		id column: 'ID_Agenda', generator: 'assigned', sqlType: 'varchar(20)'
		tipoPersona column: 'TipoPersona', sqlType: 'int'
		estado column: 'Estado', sqlType: 'numeric(18,0)'
		agendaNombre column: 'AgendaNombre', sqlType: 'varchar(250)'
		razonSocial column: 'RazonSocial', sqlType: 'varchar(250)'
		nombre column: 'Nombre', sqlType: 'varchar(50)'
		nombre2 column: 'Nombre2', sqlType: 'varchar(50)'
		apellidoPaterno column: 'ApellidoPaterno', sqlType: 'varchar(50)'
		apellidoMaterno column: 'ApellidoMaterno', sqlType: 'varchar(50)'
		ruc column: 'RUC', sqlType: 'varchar(11)'
		webPage column: 'WebPage', sqlType: 'varchar(100)'
		correoElectronico column: 'CorreoElectronico', sqlType: 'varchar(100)'
		contacto column: 'Contacto', sqlType: 'varchar(100)'
		idMonedaVta column: 'ID_MonedaVta', sqlType: 'numeric(18,0)'
		idMonedaCpra column: 'ID_MonedaCpra', sqlType: 'numeric(18,0)'
		tea column: 'TEA', sqlType: 'money'
		diasCredito column: 'DiasCredito', sqlType: 'int'
		idVendedor column: 'ID_Vendedor', sqlType: 'varchar(20)'
		tipoVendedor column: 'TipoVendedor', sqlType: 'numeric(18,0)'
		idafp column: 'IDAFP', sqlType: 'numeric(18,0)'
		idMonedaRemuneracion column: 'ID_MonedaRemuneracion', sqlType: 'numeric(18,0)'
		idCargo column: 'IDCargo', sqlType: 'numeric(18,0)'
		idSexo column: 'IDSexo', sqlType: 'numeric(18,0)'
		idGradoInstruccion column: 'IDGradoInstruccion', sqlType: 'numeric(18,0)'
		idProfesion column: 'IDProfesion', sqlType: 'numeric(18,0)'
		idEstadoCivil column: 'IDEstadoCivil', sqlType: 'numeric(18,0)'
		codigoAFP column: 'CodigoAFP', sqlType: 'varchar(50)'
		fechaIngreso column: 'FechaIngreso', sqlType: 'datetime'
		remuneracion column: 'Remuneracion', sqlType: 'money'
		conyuge column: 'Conyuge', sqlType: 'varchar(100)'
		nroHijos column: 'NroHijos', sqlType: 'int'
		codigoEmpleado column: 'CodigoEmpleado', sqlType: 'varchar(20)'
		idArea column: 'ID_Area', sqlType: 'numeric(18,0)'
		afectoPagoQuincena column: 'AfectoPagoQuincena', sqlType: 'bit'
		idDescuento column: 'ID_Descuento', sqlType: 'numeric(18,0)'
		foto column: 'Foto', sqlType: 'image'
		tarjetaNumero column: 'Tarjeta_Numero', sqlType: 'varchar(100)'
		tratamiento column: 'Tratamiento', sqlType: 'varchar(20)'
		idMedioContacto column: 'ID_Medio_Contacto', sqlType: 'numeric(18,0)'
		diaVisita column: 'Dia_Visita', sqlType: 'varchar(2)'
		nroCelularEmpresa column: 'Nro_Celular_Empresa', sqlType: 'varchar(50)'
		limiteCreditoIdMoneda column: 'Limite_Credito_ID_Moneda', sqlType: 'numeric(18,0)'
		limiteCreditoMonto column: 'Limite_Credito_Monto', sqlType: 'money'
		ruta column: 'Ruta', sqlType: 'varchar(18)'
		asignacionFamiliar column: 'Asignacion_Familiar', sqlType: 'bit'
		ipssVida column: 'IPSS_Vida', sqlType: 'bit'
		aportePacifico column: 'Aporte_Pacifico', sqlType: 'bit'
		fechaNacimiento column: 'Fecha_Nacimiento', sqlType: 'datetime'
		fechaActualizacion column: 'Fecha_Actualizacion', sqlType: 'datetime'
		aux1 column: 'Aux_1', sqlType: 'varchar(100)'
		nombreComercial column: 'Nombre_Comercial', sqlType: 'varchar(200)'
		idClas1 column: 'ID_Clas1', sqlType: 'numeric(18,0)'
		idClas2 column: 'ID_Clas2', sqlType: 'numeric(18,0)'
		idClas3 column: 'ID_Clas3', sqlType: 'numeric(18,0)'
		observaciones column: 'Observaciones', sqlType: 'varchar(200)'
		generaPuntos column: 'Genera_Puntos', sqlType: 'bit'
		idZona column: 'ID_Zona', sqlType: 'numeric(18,0)'
		esBuenContribuyente column: 'Es_Buen_Contribuyente', sqlType: 'bit'
		esAgenteRetencion column: 'Es_Agente_Retencion', sqlType: 'bit'
		ubicacion column: 'Ubicacion', sqlType: 'varchar(20)'
		tpTelefonica column: 'TP_Telefonica', sqlType: 'datetime'
		tpRegistro column: 'TP_Registro', sqlType: 'varchar(20)'
	}
}
