package com.seratic.essa.dbEssa

import java.util.Date;

class Item_Clasificador1 {

	String clasificador
	Date tpTelefonica
	String tpRegistro
	
    static constraints = {
    }
	
	static mapping = {
		datasource 'essa'
		table 'Item_Clasificador1'
		version false
		order "asc"

		id column: 'Op'
		clasificador column: 'Clasificador'
		tpTelefonica column: 'TP_Telefonica'
		tpRegistro column: 'TP_Registro'
	}
}
