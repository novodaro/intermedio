package com.seratic.essa.dbIntermedio

import java.util.Date;

class ProductoIntermedio {

	String codigoCategoria1
	String nombreCategoria1
	String codigoCategoria2
	String nombreCategoria2
	String codigoCategoria3
	String nombreCategoria3
	String codigoSucursal
	String codigoUnidad
	String codigo
	String habilitado
	String igv
	String nombre
	String precio
	String peso
	Date fechaExtraccion
	Date fechaEnvio
	Date fechaModificacionEssa
	Boolean estadoEnvio
	Boolean falloEnvio
	
    static constraints = {
		peso nullable:true, blank:true
		fechaEnvio nullable:true, blank:true
    }
	
	static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaModificacionEssa"
	}
}
