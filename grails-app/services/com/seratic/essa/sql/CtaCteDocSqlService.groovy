package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class CtaCteDocSqlService {

	def sessionFactory_essa

	def actualizaEstado(listaIdRegistrados) {
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replaceAll("\\s","").replace('[','').replace(']', '').replace(",","','")

		try{
			String query = "UPDATE CtaCte_Docs SET TP_Registro='Enviado' WHERE ID_Doc IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de CtaCte_Docs='+e.message
		}
	}

	def consultaAgendasDesactualzadas(){
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		def results = sql.rows("""select
									STUFF((
									       SELECT ',' + CAST(ccd2.ID_Doc AS VARCHAR)
									         FROM CtaCte_Docs ccd2
									        WHERE ccd2.ID_Agenda = ccd.ID_Agenda
									          FOR XML PATH('')), 1, LEN(','), '') as listaID_Doc,
											   ccd.ID_Agenda as idAgenda 
									from  CtaCte_Docs ccd
									where ccd.TP_Registro in ('Insertado', 'Actualizado')
									group by ID_Agenda
									""")
		session.clear()
		//println "resultado procedimiento: "+results
		return results
	}
}
