package com.seratic.essa.enviadores

import grails.transaction.Transactional
import com.seratic.essa.dbIntermedio.*
import grails.util.Holders
import wslite.soap.SOAPClient

class AlmacenEnviadorService {

	static transactional = false
	def actualizaEstadosIntermedioService

	def enviar() {

		def listaAlmacenIntermedio = AlmacenIntermedio.findAll(){
			estadoEnvio == false
			falloEnvio == false
		}

		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase

		def respuesta = [statusCode:200, message:'', data: []]

		def listaEnviados =[]
		def listaErrores = []

		try{

			def listaRegistrosString = ""

			println "---carga Almacen---"
			listaAlmacenIntermedio.eachWithIndex { almacenIntermedio, index ->
				try {

					def registroString = ""


					registroString = """
					<almacenes>
		               <!--Optional:-->
		               <codigo><![CDATA[${almacenIntermedio.codigo}]]></codigo>
		               <!--Optional:-->
		               <texto><![CDATA[${almacenIntermedio.nombre}]]></texto>
		            </almacenes>
				"""

					listaEnviados.add(almacenIntermedio.id)
					listaRegistrosString = listaRegistrosString + registroString
				}catch(Exception| Error e){
					println 'Error presentado en el servicio de Almacen Enviador: '+e.message
					listaErrores.add(almacenIntermedio.id)
				}
			}


			def client = new SOAPClient(urlBase+'/GestorVisitas/WSCargaMaestros?wsdl')
			def response = client.send(
					"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://com.ws.gestorvisitas.enterprise.seratic.org/">
				   <soapenv:Header/>
				   	<soapenv:Body>
				      <com:wmSincronizarAlmacenes>
				         <!--Optional:-->
				         <almacenRQT>
				            <!--Zero or more repetitions:-->
				            """+listaRegistrosString+"""
						 </almacenRQT>
					  </com:wmSincronizarAlmacenes>
				   </soapenv:Body>
				</soapenv:Envelope>"""
					)


			respuesta.statusCode = response.httpResponse.statusCode

			response.wmSincronizarAlmacenesResponse.return.resultados.errores.fila.each{e->
				listaErrores.add(listaEnviados[e.text().toInteger()-1])
			}

			respuesta.message=listaErrores.empty?"Todos los registros fueron actualizados":"Existieron registros que no se almacenaron correctamente"
			respuesta.data = listaErrores.unique()


			if(!listaErrores.empty){
				println new Date()
				println listaErrores
				println listaRegistrosString
				println response.text
			}

		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Almacenes "+ e
			respuesta.statusCode = 500
			respuesta.message = e.message

			listaErrores = listaEnviados
		}


		def diferenciaId = listaEnviados - listaErrores

		if(!diferenciaId.empty)
			actualizaEstadosIntermedioService.actualizaEstadoAlmacen(diferenciaId)

		if(listaErrores)
			actualizaEstadosIntermedioService.actualizaEstadoAlmacenFallido(listaErrores)

		return respuesta
	}
}
