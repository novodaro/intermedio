package com.seratic.essa.dbEssa

class VendedorEssa {

    String id
	String nomVendedor
	String clave
	Character estado
	String codCompania
	Character perfil
	
	
	static constraints = {
	}

	static mapping = {
		datasource 'essa'
		table 'VENDEDOR'
		version false
		order "asc"
		
		
		id column: 'vc_codvendedor', generator: 'assigned', sqlType: 'varchar(20)'
		nomVendedor column: 'vc_nomvendedor'
		clave column: 'vc_clave'
		estado column: 'ch_estado'
		codCompania column: 'vc_codcompania'
		perfil column: 'ch_perfil'
	}
}
