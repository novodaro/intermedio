package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat

@Transactional
class AgendaSqlService {

	def sessionFactory_essa
	def sessionFactory_intermedio

	def actualizaEstado(listaIdRegistrados) {
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		String stringListaIdRegistrados = listaIdRegistrados as String
		stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

		try{
			String query = "UPDATE Agenda SET TP_Registro='Enviado' WHERE ID_Agenda IN ('"+stringListaIdRegistrados+"')"
			def results = sql.executeUpdate(query)

		}
		catch(Exception e){
			println 'Error en el cambio de estado de Agenda='+e.message
		}
	}

	def cuentaCorrienta(def codCliente){
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		List results = sql.rows("{call dbo.spu_listarDocumentosDeuda($codCliente)}")
		session.clear()
		//println results


		return results
	}

	def lineaCredito(def agenda, def moneda , def fecha){
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		List results = sql.rows("{call dbo.sp_LimCredito($agenda, $moneda, $fecha)}")
		session.clear()
		//println results
		return results
	}

	def transformaAFormatoGVListas(resultadoProcedimiento){

		def cabecera = []
		def cuerpo = []
		def cuerpoString  = ""
		def cabeceraString = ""
		def patronDinero = /\d+\.\d+/
		DecimalFormatSymbols simbolos = DecimalFormatSymbols.getInstance(Locale.ENGLISH);
		NumberFormat formatter = new DecimalFormat("#0.00", simbolos);

		if(resultadoProcedimiento){
			resultadoProcedimiento.each{ fila ->
				def cuerpoFila = []
				fila.each{ columna ->
					def valorColumna = columna.value

					if(valorColumna=~patronDinero){
						def numero = valorColumna as Double
						valorColumna = formatter.format(numero)
					}

					cabecera.add(columna.key)
					cabecera = cabecera.unique()
					cuerpoString = cuerpoString + valorColumna+";"
					cuerpoFila.add(valorColumna)
				}
				cuerpoString = cuerpoString.substring(0, cuerpoString.length()-1)+ "|"

				cuerpo.add(cuerpoFila)
			}
			cabeceraString = cabecera.join(";") + "|"
			cuerpoString = cuerpoString.substring(0, cuerpoString.length()-1)
		}

		def tamanoCuerpo = cuerpoString.size()
		def limiteMaximoGV = 900-cabeceraString.size()
		if(tamanoCuerpo>limiteMaximoGV){
			//println "cuerpo normal "+cuerpoString
			def buscaPrimerSeparador = cuerpoString.indexOf("|", tamanoCuerpo-limiteMaximoGV-1)
			cuerpoString = cuerpoString.substring(buscaPrimerSeparador+1)
			//println "cuerpo modificado "+cuerpoString
		}

		def mensajeTotal = cabeceraString + cuerpoString
		return mensajeTotal

	}

	def consultaAgendasDesactualzadas(){
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		def results = sql.rows("""  select ID_Agenda as id, AgendaNombre as agendaNombre, RUC as ruc, ID_MonedaVta as idMonedaVta, direccion,
									Agenda.TP_Telefonica as tpTelefonica, FechaIngreso as fechaIngreso, ID_Vendedor as idVendedor, vc_nomvendedor as nombreVendedor
									from Agenda left join Vendedor on Vendedor.vc_codvendedor = Agenda.ID_Vendedor
									left join Direcciones on Direcciones.Op = Agenda.ID_Agenda 
									where Agenda.TP_Registro  in  ('Insertado', 'Actualizado') 
									and Agenda.RUC <> ''
									""")
		session.clear()
		//println "resultado procedimiento: "+results
		return results
	}

}
