package com.seratic.essa.enviadores

import grails.transaction.Transactional
import com.seratic.essa.dbIntermedio.*
import grails.util.Holders
import wslite.soap.SOAPClient

class RutaEnviadorService {

	static transactional = false
	def actualizaEstadosIntermedioService

	def enviar(listaRutaIntermedio) {

		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase

		def respuesta = [statusCode:200, message:'', data: []]

		def listaEnviados =[]
		def listaErrores = []

		try{

			def listaRutasString = ""

			println "---carga Rutas---"
			listaRutaIntermedio.eachWithIndex { rutaIntermedio, index ->

				try{

					def rutaString = ""

					if(rutaIntermedio.idVendedor){
						rutaString = """
					<rutas>
		               <!--Optional:-->
		               <codPuntoVenta><![CDATA[${rutaIntermedio.idAgenda}]]></codPuntoVenta>
		               <!--Optional:-->
		               <fechasRuta></fechasRuta>
		               <orden></orden>
		               <permanente>1</permanente>
		               <!--Optional:-->
		               <usuario><![CDATA[${rutaIntermedio.nombreVendedor}]]></usuario>
		            </rutas>

				"""
					}

					listaEnviados.add(rutaIntermedio.id)
					listaRutasString = listaRutasString + rutaString

				}catch(Exception| Error e){
					println 'Error presentado en el servicio de Ruta Enviador: '+e.message
					listaErrores.add(rutaIntermedio.id)
				}

			}

			def client = new SOAPClient(urlBase+'/GestorVisitas/WSCargaMaestros?wsdl')
			def response = client.send(
					"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://com.ws.gestorvisitas.enterprise.seratic.org/">
				   <soapenv:Header/>
				   	<soapenv:Body>
				      <com:wmSincronizarRutas>
				         <!--Optional:-->
				         <rutasRQT>
				            <!--Zero or more repetitions:-->
				            """+listaRutasString+"""
							 </rutasRQT>
				      </com:wmSincronizarRutas>
				   </soapenv:Body>
				</soapenv:Envelope>"""
					)


			respuesta.statusCode = response.httpResponse.statusCode

			response.wmSincronizarRutasResponse.return.resultados.errores.fila.each{e->
				listaErrores.add(listaEnviados[e.text().toInteger()-1])
			}

			respuesta.message=listaErrores.empty?"Todos los registros fueron actualizados":"Existieron registros que no se almacenaron correctamente"
			respuesta.data = listaErrores.unique()

			if(!listaErrores.empty){
				println new Date()
				println listaErrores
				//println listaRutasString
				println response.text
			}

		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Rutas "+ e
			respuesta.statusCode = 500
			respuesta.message = e.message

			listaErrores = listaEnviados
			println respuesta
		}


		def diferenciaId = listaEnviados - listaErrores

		if(diferenciaId)
			actualizaEstadosIntermedioService.actualizaEstadoRuta(diferenciaId)

		if(listaErrores)
			actualizaEstadosIntermedioService.actualizaEstadoRutaFallido(listaErrores)

		println respuesta
		return respuesta

	}

	def cargaMultiple(max){

		def respuesta = [:]

		def listaRegistrosNoActualizados = []

		def status = 200
		def listaIdRegistros = []

		def totalRegistros = 0

		while(true){

			def listaRegistros =   RutaIntermedio.findAll([max: max]){
				estadoEnvio == false
				falloEnvio == false
			}

			listaIdRegistros.addAll(listaRegistros.id)

			def respuestaTemporal = enviar(listaRegistros)

			listaRegistrosNoActualizados.add(respuestaTemporal.data)
			status = respuestaTemporal.statusCode!=200?respuestaTemporal.statusCode:status

			if(!listaRegistros)break
		}

		listaRegistrosNoActualizados = listaRegistrosNoActualizados.flatten()
		totalRegistros = listaIdRegistros.size()

		def registrosActualizadosTotal = totalRegistros-listaRegistrosNoActualizados.size()

		respuesta.status = status
		respuesta.message = 'Se registraron '+registrosActualizadosTotal+' de '+totalRegistros
		respuesta.data = listaRegistrosNoActualizados

		return respuesta
	}
}
