package com.seratic.essa.dbEssa

class ItemCodigoEssa {

    String idItem
	String descripcion
	String unidad
	Float factorConversion
	Float precio
	Long idMondea
	Float dctoMax
	Float margen
	Float comision
	String prsentacion
	Float stock
	String idItemAux
	Long idMarca
	Item_Clasificador1 idClas1
	Item_Clasificador2 idClas2
	Item_Clasificador3 idClas3
	Float eqBarriles
	Float eqGalones
	Float eqBaldes
	Long idUnidad
	Float stockMinimo
	Float stockMaximo
	Float stockReposicion
	Float largo
	Float ancho
	Float espesor
	Long opSubFamilia
	Long opLinea
	Float eqDu
	Boolean du
	Boolean activo
	Float ctoSoles
	Float ctoDolares
	Date tpTelefonica
	String tpRegistro


	static constraints = {

	}

	static mapping = {
		datasource 'essa'
		table 'ItemCodigo'
		version false
		order "asc"

		id column: 'ID'
		idItem column: 'ID_Item', sqlType:'varchar(20)'
		descripcion column: 'Descripcion'
		unidad column: 'Unidad'
		factorConversion column: 'FactorConversion', sqlType: 'money'
		precio column: 'Precio', sqlType: 'money'
		idMondea column: 'ID_Moneda'
		dctoMax column: 'DctoMax', sqlType: 'money'
		margen column: 'Margen', sqlType: 'money'
		comision column: 'Comision', sqlType: 'money'
		prsentacion column: 'Presentacion'
		stock column: 'Stock', sqlType: 'money'
		idItemAux column: 'ID_Item_Aux'
		idMarca column: 'ID_Marca'
		idClas1 column: 'ID_Clas1'
		idClas2 column: 'ID_Clas2'
		idClas3 column: 'ID_Clas3'
		eqBarriles column: 'Eq_Barriles', sqlType: 'money'
		eqGalones column: 'Eq_Galones', sqlType: 'money'
		eqBaldes column: 'Eq_Baldes', sqlType: 'money'
		idUnidad column: 'ID_Unidad'
		stockMinimo column: 'Stock_Minimo', sqlType: 'money'
		stockMaximo column: 'Stock_Maximo', sqlType: 'money'
		stockReposicion column: 'Stock_Reposicion', sqlType: 'money'
		largo column: 'Largo', sqlType: 'money'
		ancho column: 'Ancho', sqlType: 'money'
		espesor column: 'Espesor', sqlType: 'money'
		opSubFamilia column: 'Op_Sub_Familia'
		opLinea column: 'Op_Linea'
		eqDu column: 'Eq_DU', sqlType: 'money'
		du column: 'DU'
		activo column: 'Activo'
		ctoSoles column: 'Cto_Soles', sqlType: 'money'
		ctoDolares column: 'Cto_Dolares', sqlType: 'money'
		tpTelefonica column: 'TP_Telefonica', sqlType: 'datetime'
		tpRegistro column: 'TP_Registro', sqlType: 'varchar(20)'
	}
}
