import com.seratic.essa.dbIntermedio.VariableIntermedio
import com.seratic.essa.*

class BootStrap {

    def init = { servletContext ->
		if(!VariableIntermedio.findByNombre('ultimaDescargaPedidos')){
			def ultimaDescarga = new VariableIntermedio(nombre: 'ultimaDescargaPedidos', valor: "2017/11/10T16:05:00")
			ultimaDescarga.save(flush:true)
		}
		
		def cronExpresionMaestros = VariableIntermedio.findByNombre("cronExpresionMaestros")
		
		if(!cronExpresionMaestros){
			cronExpresionMaestros = new VariableIntermedio(nombre:"cronExpresionMaestros", valor: "0 0 0 1/1 * ? *")
			cronExpresionMaestros.save(flush:true)
		}
		
		def cronExpresionLimpiador = VariableIntermedio.findByNombre("cronExpresionLimpiador")
		
		if(!cronExpresionLimpiador){
			cronExpresionLimpiador = new VariableIntermedio(nombre:"cronExpresionLimpiador", valor: "0 0 0 1/1 * ? *")
			cronExpresionLimpiador.save(flush:true)
		}
		
		MaestrosJob.schedule(cronExpresionMaestros.valor, [:])
		LimpiadorJob.schedule(cronExpresionLimpiador.valor, [:])
		
    }
    def destroy = {
    }
}
