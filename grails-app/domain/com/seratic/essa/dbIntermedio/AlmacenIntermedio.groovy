package com.seratic.essa.dbIntermedio

import java.util.Date;

class AlmacenIntermedio {

	String nombre
	String codigo
	Date fechaExtraccion
	Date fechaEnvio
	Boolean estadoEnvio
	Boolean falloEnvio
	
	static constraints = {
		fechaEnvio nullable:true, blank:true
	}
	
    static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaExtraccion"
	}
}
