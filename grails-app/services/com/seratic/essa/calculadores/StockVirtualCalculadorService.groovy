package com.seratic.essa.calculadores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

@Transactional
class StockVirtualCalculadorService {

	def validarEstadoProceso() {
		def listaCabeceraPedidoIntermedio = CabeceraPedidoIntermedio.findAll(){
			estadoEnvio == true
			procesado == false
		}
		

		listaCabeceraPedidoIntermedio.each{ cabeceraPedidoIntermedio->
			def cabeceraPedidoEssa = CabeceraPedidoEssa.get(cabeceraPedidoIntermedio.idPedidoEssa as Long)
			if(cabeceraPedidoEssa.procesado&&(!cabeceraPedidoIntermedio.procesado)){
				cabeceraPedidoIntermedio.procesado = cabeceraPedidoEssa.procesado
				cabeceraPedidoIntermedio.validoParaCalculoStockVirtual = false
				if(!cabeceraPedidoIntermedio.save(flush:true)){
					println "Se presenta inconveniente al cambiar estado validoParaCalculoStockVirtual a false en cabeceraPedidoIntermedio: "+cabeceraPedidoIntermedio.id
				}
			}
		}

	}

	def calcularStockVirtual(){

		validarEstadoProceso()

		def listaCabeceraPedidoIntermedio = CabeceraPedidoIntermedio.findAll(){
			validoParaCalculoStockVirtual == true
		}

		if(listaCabeceraPedidoIntermedio){
			String stringListaIdRegistrados = listaCabeceraPedidoIntermedio.id as String
			stringListaIdRegistrados=stringListaIdRegistrados.replace('[','').replace(']', '').replace(", ","','")

			def groupoDetallePedidoIntermedio = DetallePedidoIntermedio.executeQuery("""select codArticulo as producto, almacen, sum(cantidad)"""+
					"""from DetallePedidoIntermedio dpi where idCabeceraPedidoIntermedio in ('"""+ stringListaIdRegistrados+"""') group by dpi.codArticulo, dpi.almacen""")

			groupoDetallePedidoIntermedio.each{ g ->
				def stockIntermedio = StockIntermedio.findByCodigoProductoAndCodigoAlmacen(g[0], g[1])

				if(stockIntermedio){
					stockIntermedio.stockVirtual = stockIntermedio.stock - (g[2] as Long)
					stockIntermedio.save(flush:true)
				}else{
					println "No existe un stock con la siguiente combinación producto: "+g[0]+" , almacen: "+g[1]
				}

			}
		}

	}
}
