package com.seratic.essa.dbIntermedio

class VariableIntermedio {

    String nombre
	String valor
	
    static constraints = {
    }
	
	static mapping = {
		datasource 'intermedio'
	}
}
