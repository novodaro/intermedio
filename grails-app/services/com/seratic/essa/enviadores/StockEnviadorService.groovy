package com.seratic.essa.enviadores

import grails.transaction.Transactional
import com.seratic.essa.dbIntermedio.*
import grails.util.Holders
import wslite.soap.SOAPClient

class StockEnviadorService {

	static transactional = false
	def actualizaEstadosIntermedioService

	def enviar(listaStockIntermedio) {

		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase

		def respuesta = [statusCode:200, message:'', data: []]

		def listaEnviados =[]
		def listaErrores = []

		try{
			def listaStocksString = ""

			println "---carga Stock---"
			listaStockIntermedio.each{ stockIntermedio ->

				try{
					def stockString = """
						 <stocksAlmacen>
			               <!--Optional:-->
			               <codAlmacen><![CDATA[${stockIntermedio.codigoAlmacen}]]></codAlmacen>
			               <!--Optional:-->
			               <codigo><![CDATA[${stockIntermedio.codigoProducto}]]></codigo>
			               <stock><![CDATA[${stockIntermedio.stockVirtual?:stockIntermedio.stock}]]></stock>
			               <!--Optional:-->
			               <unidad><![CDATA[${stockIntermedio.unidadProducto}]]></unidad>
			            </stocksAlmacen>
					"""
					listaEnviados.add(stockIntermedio.id)
					listaStocksString = listaStocksString + stockString
					
				}catch(Exception| Error e){
					println 'Error presentado en el servicio de Stock Enviador: '+e.message
					listaErrores.add(stockIntermedio.id)
				}
			}


			def client = new SOAPClient(urlBase+'/GestorVisitas/WSCargaMaestros?wsdl')
			def response = client.send(
					"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://com.ws.gestorvisitas.enterprise.seratic.org/">
					   <soapenv:Header/>
					   <soapenv:Body>
					   		<com:wmSincronizarStockAlmacen>
						         <!--Optional:-->
						         <stockAlmacenRQT>
						            <!--Zero or more repetitions:-->
						            """+listaStocksString+"""
										 </stockAlmacenRQT>
									  </com:wmSincronizarStockAlmacen>
							   </soapenv:Body>
							</soapenv:Envelope>"""
					)

			respuesta.statusCode = response.httpResponse.statusCode

			response.wmSincronizarStockAlmacenResponse.return.resultados.errores.fila.each{e->
				listaErrores.add(listaEnviados[e.text().toInteger()-1])
			}

			respuesta.message=listaErrores.empty?"Todos los registros fueron actualizados":"Existieron registros que no se almacenaron correctamente"
			respuesta.data = listaErrores.unique()

			if(!listaErrores.empty){
				println new Date()
				println listaErrores
				println listaStocksString
				println response.text
			}

		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Stock "+ e
			respuesta.statusCode = 500
			respuesta.message = e.message
			
			listaErrores = listaEnviados
		}

		def diferenciaId = listaEnviados - listaErrores

		if(diferenciaId)
			actualizaEstadosIntermedioService.actualizaEstadoStock(diferenciaId)

		if(listaErrores)
			actualizaEstadosIntermedioService.actualizaEstadoStockFallido(listaErrores)

		println respuesta
		return respuesta
	}

	def cargaMultiple(max){

		def respuesta = [:]

		def listaRegistrosNoActualizados = []

		def status = 200
		def listaIdRegistros = []

		def totalRegistros = 0

		while(true){

			def listaRegistros =   StockIntermedio.findAll([max: max]){
				estadoEnvio == false
				falloEnvio == false
			}

			listaIdRegistros.addAll(listaRegistros.id)

			def respuestaTemporal = enviar(listaRegistros)

			listaRegistrosNoActualizados.add(respuestaTemporal.data)
			status = respuestaTemporal.statusCode!=200?respuestaTemporal.statusCode:status

			if(!listaRegistros)break
		}

		listaRegistrosNoActualizados = listaRegistrosNoActualizados.flatten()
		totalRegistros = listaIdRegistros.size()

		def registrosActualizadosTotal = totalRegistros-listaRegistrosNoActualizados.size()

		respuesta.status = status
		respuesta.message = 'Se registraron '+registrosActualizadosTotal+' de '+totalRegistros
		respuesta.data = listaRegistrosNoActualizados

		return respuesta
	}
}
