package com.seratic.essa.enviadores

import grails.transaction.Transactional
import wslite.soap.SOAPClient
import com.seratic.essa.dbIntermedio.*
import grails.util.Holders

class ProductoEnviadorService {

	static transactional = false
	def actualizaEstadosIntermedioService


	def enviar(listaProductoIntermedio) {
		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase


		def respuesta = [statusCode:200, message:'', data: []]

		def listaEnviados =[]
		def listaErrores = []

		try{

			println "---carga Productos---"+new Date()
			def listaProductosString = ""

			listaProductoIntermedio.eachWithIndex { productoIntermedio, index ->

				try{

					def productoString = """
					 <productos>
			               <aporteCuota></aporteCuota>
			               <!--Zero or more repetitions:-->
			               <categorias>
			                  <!--Optional:-->
			                  <codCategoria><![CDATA[${productoIntermedio.codigoCategoria1}]]></codCategoria>
			                  <!--Optional:-->
			                  <nomCategoria><![CDATA[${productoIntermedio.nombreCategoria1}]]></nomCategoria>
			                  <!--Optional:-->
			                  <posicion></posicion>
			                  <!--Optional:-->
			                  <tipoCategoria></tipoCategoria>
			               </categorias>
						   <categorias>
			                  <!--Optional:-->
			                   <codCategoria><![CDATA[${productoIntermedio.codigoCategoria2}]]></codCategoria>
			                  <!--Optional:-->
			                  <nomCategoria><![CDATA[${productoIntermedio.nombreCategoria2}]]></nomCategoria>
			                  <!--Optional:-->
			                  <posicion></posicion>
			                  <!--Optional:-->
			                  <tipoCategoria></tipoCategoria>
			               </categorias>
							  <categorias>
			                  <!--Optional:-->
			                   <codCategoria><![CDATA[${productoIntermedio.codigoCategoria3}]]></codCategoria>
			                  <!--Optional:-->
			                  <nomCategoria><![CDATA[${productoIntermedio.nombreCategoria3}]]></nomCategoria>
			                  <!--Optional:-->
			                  <posicion></posicion>
			                  <!--Optional:-->
			                  <tipoCategoria></tipoCategoria>
			               </categorias>
						   
			               <codSucursal>1</codSucursal>
			               <!--Optional:-->
			               <codUnidad><![CDATA[${productoIntermedio.codigoUnidad}]]></codUnidad>
			               <!--Optional:-->
			               <codigo><![CDATA[${productoIntermedio.codigo}]]></codigo>
			               <habilitado><![CDATA[${productoIntermedio.habilitado}]]></habilitado>
			               <!--Optional:-->
			               <igv><![CDATA[${productoIntermedio.igv}]]></igv>
			               <!--Optional:-->
			               <nombre><![CDATA[${productoIntermedio.nombre}]]></nombre>
			               <!--Optional:-->
			               <peso></peso>
			               <precio><![CDATA[${productoIntermedio.precio}]]></precio>
			            </productos>

			"""
					listaEnviados.add(productoIntermedio.id)
					listaProductosString = listaProductosString + productoString
					
				}catch(Exception| Error e){
					println 'Error presentado en el servicio de Producto Enviador: '+e.message
					listaErrores.add(productoIntermedio.id)
				}

			}

			//println "INICIO ENVIO "+ new Date()
			def client = new SOAPClient(urlBase+'/GestorVisitas/WSCargaMaestros?wsdl')
			def response = client.send(
					"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://com.ws.gestorvisitas.enterprise.seratic.org/">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <com:wmSincronizarProductos>
					         <!--Optional:-->
					         <productoRQT>
					            <!--Zero or more repetitions:-->
				            """+listaProductosString+"""
										</productoRQT>
									  </com:wmSincronizarProductos>
								   </soapenv:Body>
								</soapenv:Envelope>"""
					)

			respuesta.statusCode = response.httpResponse.statusCode

			//println "FIN ENVIO "+ new Date()

			response.wmSincronizarProductosResponse.return.resultados.errores.fila.each{e->
				listaErrores.add(listaEnviados[e.text().toInteger()-1])
			}



			respuesta.message=listaErrores.empty?"Todos los registros fueron actualizados":"Existieron registros que no se almacenaron correctamente"
			respuesta.data = listaErrores.unique()

			if(!listaErrores.empty){
				println new Date()
				println listaErrores
				println response.text
			}

		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Productos "+ e
			respuesta.statusCode = 500
			respuesta.message = e.message
			
			listaErrores= listaEnviados
		}

		def diferenciaId = listaEnviados - listaErrores

		if(diferenciaId)
			actualizaEstadosIntermedioService.actualizaEstadoProducto(diferenciaId)

		if(listaErrores)
			actualizaEstadosIntermedioService.actualizaEstadoProductoFallido(listaErrores)

		println respuesta
		println "---Finaliza carga Productos---"+new Date()
		return respuesta
	}

	def cargaMultiple(max){

		def respuesta = [:]

		def listaRegistrosNoActualizados = []

		def status = 200
		def listaIdRegistros = []

		def totalRegistros = 0

		while(true){

			def listaRegistros =   ProductoIntermedio.findAll([max: max]){
				estadoEnvio == false
				falloEnvio == false
			}

			listaIdRegistros.addAll(listaRegistros.id)

			def respuestaTemporal = enviar(listaRegistros)

			listaRegistrosNoActualizados.add(respuestaTemporal.data)
			status = respuestaTemporal.statusCode!=200?respuestaTemporal.statusCode:status

			if(!listaRegistros)break
		}

		listaRegistrosNoActualizados = listaRegistrosNoActualizados.flatten()
		totalRegistros = listaIdRegistros.size()

		def registrosActualizadosTotal = totalRegistros-listaRegistrosNoActualizados.size()

		respuesta.status = status
		respuesta.message = 'Se registraron '+registrosActualizadosTotal+' de '+totalRegistros
		respuesta.data = listaRegistrosNoActualizados

		return respuesta

	}
}
