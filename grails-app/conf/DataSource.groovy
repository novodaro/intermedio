dataSource {
	pooled = true
}
hibernate {
	cache.use_second_level_cache = false
	cache.use_query_cache = false
	//    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
	cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
	singleSession = true // configure OSIV singleSession mode
	flush.mode = 'manual' // OSIV session flush mode outside of transactional context
}

// environment specific settings
environments {
	development {
		//Conexión a Base de Datos de Essa
		dataSource_essa {
			dbCreate = "validate" // one of 'create', 'create-drop', 'update', 'validate', ''
			//url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
			url = "jdbc:jtds:sqlserver://127.0.0.1:1433/Essa_2017"
			username = "leonardo"
			password = "leonardo"
			driverClassName = "net.sourceforge.jtds.jdbc.Driver"
			dialect = "org.hibernate.dialect.SQLServerDialect"
		}

		//Conexión a Base de Datos Interna, usada para registrar actualizaciones
		dataSource_intermedio {
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			//url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
			url = "jdbc:jtds:sqlserver://127.0.0.1:1433/INTERMEDIO_GV_ESSA2"
			username = "leonardo"
			password = "leonardo"
			driverClassName = "net.sourceforge.jtds.jdbc.Driver"
			dialect = "org.hibernate.dialect.SQLServerDialect"
		}
	}
	test {
		dataSource {
			dbCreate = "update"
			url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
		}
	}
	production {
		dataSource_essa {
			dbCreate = "validate" // one of 'create', 'create-drop', 'update', 'validate', ''
			//url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
			url = ""
			username = ""
			password = ""
			driverClassName = "net.sourceforge.jtds.jdbc.Driver"
			dialect = "org.hibernate.dialect.SQLServerDialect"
		}

		//Conexión a Base de Datos Interna, usada para registrar actualizaciones
		dataSource_intermedio {
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			url = ""
			username = ""
			password = ""
			driverClassName = "net.sourceforge.jtds.jdbc.Driver"
			dialect = "org.hibernate.dialect.SQLServerDialect"
		}
	}
}
