package com.seratic.essa.dbIntermedio

import java.util.Date;

class RutaIntermedio {

	String idAgenda
	String idVendedor
	Date fechaExtraccion
	Date fechaEnvio
	Date fechaModificacionEssa
	Boolean estadoEnvio
	String nombreVendedor
	Boolean falloEnvio
	
    static constraints = {
		fechaEnvio nullable:true, blank:true
		idVendedor nullable:true, blank:true
		nombreVendedor nullable: true, blank:true
    }
	
	static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaModificacionEssa"
	}
}
