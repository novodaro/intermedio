package com.seratic.essa.extractores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*
import com.seratic.essa.utils.*

class StockRealExtractorService {

	static transactional = false
	def stockRealSqlService
	def inventario_MovSqlService
	def sessionFactory_intermedio

	//Este metodo debe ser llamado en el momento en que se vaya a realizar el cargue de un producto.
	def extraer() {
		//def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()

		//TODO se debe colocar una restriccion adicional para que no traiga registros que no deben tenerse en cuenta en el stock
		//def listaInventario = Inventario_Mov.findAll(){
		//	tpRegistro in  ['Insertado', 'Actualizado']
		//}
		
		def listaIdRegistrados = []

		//def listaCodigoProducto = listaInventario.idItem.unique()
		
		def listaStock = inventario_MovSqlService.spu_buscarStock()

		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		listaStock.eachWithIndex {stock, index->

			def stockRealIntermedio = StockIntermedio.findByCodigoProductoAndCodigoAlmacen(stock.CodProducto, stock.ID_Almacen)?:new StockIntermedio()
			stockRealIntermedio.codigoProducto = stock.CodProducto
			stockRealIntermedio.codigoAlmacen = stock.ID_Almacen
			stockRealIntermedio.stock = stock.stock as Long
			stockRealIntermedio.precio = stock.precio as Long
			stockRealIntermedio.moneda = stock.id_moneda as Long
			stockRealIntermedio.fechaExtraccion = new Date()
			stockRealIntermedio.fechaModificacionEssa = new Date()
			stockRealIntermedio.estadoEnvio = false
			stockRealIntermedio.unidadProducto = stock.UnidadProducto
			stockRealIntermedio.falloEnvio = false

			if(stockRealIntermedio.save(flush:true)){
				def listaId = Utilidad.clobToString(stock.listaInventarios)
				listaIdRegistrados.addAll(listaId)
				if(index.mod(100)==0) {
					cleanUpGorm()
				}
			}

		}
		
		//Generalmente se estaran tratando de actualizar registros que ya han sido cargados debido a que el stock de un producto se calcula con multiples registros
		if(listaIdRegistrados){
			inventario_MovSqlService.actualizaEstado(listaIdRegistrados)
		}

		println "-----FINALIZA REGISTRO: "+new Date()

	}
	
	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
