package com.seratic.essa.dbEssa

class CabeceraPedidoEssa {

	String codCompania
	String codVendedor
	String codCliente
	String codCondventa
	Character codMoneda
	Float tipoCambio
	String direccionEntrega
	String glosa
	Date fechaRegistro
	Boolean procesado
	String codPedidoBk

	static constraints = {
		codCompania size: 1..2
		codVendedor size: 1..4
		codCliente size: 1..20
		codCondventa size: 1..2
		codMoneda size: 1..1
		direccionEntrega nullable: true, blank:true, size: 0..100
		glosa nullable:true, blank:true, size: 0..100
		fechaRegistro nullable:true, blank:true
		codPedidoBk nullable:true, blank:true, size: 0..50
	}

	static mapping = {
		datasource 'essa'
		table 'CABECERA_PEDIDO'
		version false
		order "asc"

		id column: 'vc_codpedido'
		codCompania column: 'vc_codcompania'
		codVendedor column: 'vc_codvendedor'
		codCliente column: 'vc_codcliente'
		codCondventa column: 'vc_codcondventa'
		codMoneda column: 'ch_codmoneda'
		tipoCambio column: 'flt_tipocambio'
		direccionEntrega column: 'vc_direccionentrega'
		glosa column: 'vc_glosa'
		fechaRegistro column: 'dt_fecharegistro'
		procesado column: 'Procesado'
		codPedidoBk column: 'vc_codpedido_bk'
	}
}
