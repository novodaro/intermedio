package com.seratic.essa.extractores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

class AlmacenExtractorService {

	static transactional = false
    def almacenSqlService
	def sessionFactory_intermedio
	
    def extraer() {
		
		def listaIdRegistrados = []
		
		println "-----INICIA CONSULTA: "+new Date()
		def listaAlmacen = almacenSqlService.sp_ListaAlmacen()
		println "-----Finaliza CONSULTA: "+new Date()
		
		println "-----INICIA REGISTRO: "+new Date()
		
		listaAlmacen.eachWithIndex { almacen, index ->
			def almacenIntermedio = AlmacenIntermedio.findByCodigo(almacen.ID)?:new AlmacenIntermedio()
			almacenIntermedio.nombre = almacen.Nombre
			almacenIntermedio.codigo = almacen.ID
			almacenIntermedio.fechaExtraccion = new Date()
			almacenIntermedio.estadoEnvio = false
			almacenIntermedio.falloEnvio = false
			
			if(almacenIntermedio.save(flush:true)){
				listaIdRegistrados.add(almacen.ID)
				if(index.mod(100)==0) {
					cleanUpGorm()
				}
			}else{
				println 'Ocurrio un inconveniente al tratar de guardar en el intermedio el almacen: '+ almacen.ID
				println almacenIntermedio.errors
			}
		}
		
		println "-----FINALIZA REGISTRO: "+new Date()
    }
	
	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
