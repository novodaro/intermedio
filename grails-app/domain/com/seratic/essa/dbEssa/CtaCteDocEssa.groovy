package com.seratic.essa.dbEssa

class CtaCteDocEssa {

   Long idCaja
	String idUsuario
	Date fecha
	Date vcmto
	String idAgenda
	Long idAgendaAnexo
	String tablaOrigen
	Long op
	Long idMoneda
	Float entrada
	Float salida
	Integer origen
	String serie
	Long numero
	Boolean signo
	Float pendiente
	String glosa
	Float importe
	Float cancelado
	Float financiado
	Float enProceso
	Long estado
	Float renovado
	Date actualizado
	Float tea
	Float intereses
	Float aplicar
	Long idDocumento
	Float retencion
	Float retencionTerceros
	Long idTipoNegocio
	Long auxEstado
	Long idCtoDigitacion
	String idVendedor
	Boolean afectoDetraccion
	Date tpTelefonica
	String tpRegistro
	
	
    static constraints = {
    }
	
	static mapping = {
		datasource 'essa'
		table 'CtaCte_Docs'
		version false
		order "asc"
		
		id column: 'ID_Doc', sqlType: 'numeric(18,0)'
		idCaja column: 'ID_Caja', sqlType: 'numeric(18,0)'
		idUsuario column: 'ID_Usuario', sqlType: 'varchar(20)'
		fecha column: 'Fecha', sqlType: 'datetime'
		vcmto column: 'Vcmto', sqlType: 'datetime'
		idAgenda column: 'ID_Agenda', sqlType: 'varchar(20)'
		idAgendaAnexo column: 'ID_AgendaAnexo', sqlType: 'numeric(18,0)'
		tablaOrigen column: 'TablaOrigen', sqlType: 'varchar(30)'
		op column: 'Op', sqlType: 'numeric(18,0)'
		idMoneda column: 'ID_Moneda', sqlType: 'numeric(18,0)'
		entrada column: 'Entrada', sqlType: 'money'
		salida column: 'Salida', sqlType: 'money'
		origen column: 'Origen', sqlType: 'int'
		serie column: 'Serie', sqlType: 'varchar(20)'
		numero column: 'Numero', sqlType: 'numeric(18,0)'
		signo column: 'Signo', sqlType: 'bit'
		pendiente column: 'Pendiente', sqlType: 'money'
		glosa column: 'Glosa', sqlType: 'varchar(400)'
		importe column: 'Importe', sqlType: 'money'
		cancelado column: 'Cancelado', sqlType: 'money'
		financiado column: 'Financiado', sqlType: 'money'
		enProceso column: 'EnProceso', sqlType: 'money'
		estado column: 'Estado', sqlType: 'numeric(18,0)'
		renovado column: 'Renovado', sqlType: 'money'
		actualizado column: 'Actualizado', sqlType: 'datetime'
		tea column: 'TEA', sqlType: 'money'
		intereses column: 'Intereses', sqlType: 'money'
		aplicar column: 'Aplicar', sqlType: 'money'
		idDocumento column: 'ID_Documento', sqlType: 'numeric(18,0)'
		retencion column: 'Retencion', sqlType: 'money'
		retencionTerceros column: 'RetencionTerceros', sqlType: 'money'
		idTipoNegocio column: 'IDTipoNegocio', sqlType: 'numeric(18,0)'
		auxEstado column: 'AuxEstado', sqlType: 'numeric(18,0)'
		idCtoDigitacion column: 'ID_CtoDigitacion', sqlType: 'numeric(18,0)'
		idVendedor column: 'ID_Vendedor', sqlType: 'varchar(20)'
		afectoDetraccion column: 'Afecto_Detraccion', sqlType: 'bit'
		tpTelefonica column: 'TP_Telefonica', sqlType: 'datetime'
		tpRegistro column: 'TP_Registro', sqlType: 'varchar(20)'
		
		
	}
}
