package com.seratic.essa.sql

import grails.transaction.Transactional
import groovy.sql.Sql

@Transactional
class Inventario_MovSqlService {

	def sessionFactory_essa

	def spu_buscarStock() {

		//String stringListaProductos = listaProductos as String
		//stringListaProductos=stringListaProductos.replace('[','').replace(']', '').replace(", ","','")

		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())
		def results = sql.rows("""
									Select STUFF((
										       SELECT ',' + CAST(a.Op AS VARCHAR)
										         FROM Inventario_Mov a
										        WHERE a.ID_Item = b.ID_Item
												     and a.ID_Almacen = b.ID_Almacen
										          FOR XML PATH('')), 1, LEN(','), '') as listaInventarios, 
											 ISNULL(NullIf(ItemCodigo.unidad, ''), 'UND') as UnidadProducto, 
										b.ID_Item As CodProducto, b.ID_Almacen, AgendaNombre As Almacen, 
										Sum((Case EsEntrada When 1 Then 1 Else -1 End)*Cantidad) As stock, ItemCodigo.precio, ItemCodigo.id_moneda 
										From Inventario_Mov b
										Inner Join ItemCodigo On b.ID_Item = ItemCodigo.ID_Item
										Inner Join Agenda On b.ID_Almacen = Agenda.ID_Agenda
										Where b.ID_Item in (select ID_Item from Inventario_Mov im2 where im2.TP_Registro in ('Insertado', 'Actualizado'))
										Group By ItemCodigo.unidad, b.ID_Item, b.ID_Almacen, Agenda.AgendaNombre, ItemCodigo.Precio, ItemCodigo.ID_Moneda
										order by CodProducto
											""")
		session.clear()
		//println "resultado procedimiento: "+results
		return results
	}

	def actualizaEstado(listaIdRegistrados) {
		def session = sessionFactory_essa.currentSession
		def sql = new Sql(session.connection())

		def subListaRegistrados = []
		def contador = 0

		for(registro in listaIdRegistrados){

			subListaRegistrados.add(registro)
			contador++

			if((contador%100==0)||(contador==listaIdRegistrados.size())){

				String stringListaIdRegistrados = subListaRegistrados as String
				stringListaIdRegistrados=stringListaIdRegistrados.replaceAll("\\s","").replace('[','').replace(']', '').replace(",","','")

				try{
					String query = "UPDATE Inventario_Mov SET TP_Registro='Enviado' WHERE Op IN ('"+stringListaIdRegistrados+"') and TP_Registro in ('Insertado', 'Actualizado')"
					def results = sql.executeUpdate(query)
					subListaRegistrados = []

				}
				catch(Exception e){
					println 'Error en el cambio de estado de Inventario_Mov ='+e.message
				}
			}

		}
	}
}
