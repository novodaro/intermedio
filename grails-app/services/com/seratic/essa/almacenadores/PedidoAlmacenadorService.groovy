package com.seratic.essa.almacenadores

import grails.transaction.Transactional
import groovy.sql.Sql
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

@Transactional
class PedidoAlmacenadorService {

	def sessionFactory_essa

	def almacenar() {
		def listaCabeceraPedido = CabeceraPedidoIntermedio.findAll(){
			estadoEnvio == false
			tieneErrorDetalle == false
		}

		listaCabeceraPedido.each{ cabeceraPedidoIntermedio ->
			def cabeceraPedidoEssa = new CabeceraPedidoEssa()
			cabeceraPedidoEssa.properties = cabeceraPedidoIntermedio.properties

			if(cabeceraPedidoEssa.save(flush:true)){
				cabeceraPedidoIntermedio.estadoEnvio = true
				cabeceraPedidoIntermedio.fechaEnvio = new Date()
				cabeceraPedidoIntermedio.idPedidoEssa = cabeceraPedidoEssa.id

				if(cabeceraPedidoIntermedio.save(flush:true)){
					def listaDetallePedidoIntermedio = DetallePedidoIntermedio.findAllByIdCabeceraPedidoIntermedio(cabeceraPedidoIntermedio.id)

					listaDetallePedidoIntermedio.each{detallePedidoIntermedio->
						def detallePedidoEssa = new DetallePedidoEssa()
						detallePedidoEssa.properties = detallePedidoIntermedio.properties
						detallePedidoEssa.codPedido = cabeceraPedidoEssa.id

						if(insertaDetallePedido(detallePedidoEssa)){
							detallePedidoIntermedio.estadoEnvio = true
							detallePedidoIntermedio.fechaEnvio = new Date()
							if(!detallePedidoIntermedio.save(flush:true)){
								println 'Hubo un inconveniente al cambiar el estado de la detallePedidoIntermedio: '+detallePedidoIntermedio.id
							}
						}else{
							println 'Hubo un inconveniente al guardar en BD Essa el detallePedidoIntermedio: '+detallePedidoIntermedio.id
							detallePedidoEssa.errors
						}
					}
				}else{
					println 'Hubo un inconveniente al cambiar el estado de la cabeceraPedidoIntermedio: '+cabeceraPedidoIntermedio.id
				}

			}else{
				println 'Hubo un inconveniente al guardar en BD Essa el cabeceraPedidoIntermedio: '+cabeceraPedidoIntermedio.id
			}
		}

	}

	def insertaDetallePedido(def detallePedido){
		try{
			def session = sessionFactory_essa.currentSession
			def sql = new Sql(session.connection())
			def results = sql.execute("Set IDENTITY_INSERT DETALLE_PEDIDO on")
			def resultado = sql.execute("""
				INSERT INTO DETALLE_PEDIDO([vc_codcompania],[vc_codpedido]
			      ,[smi_orden]
			      ,[vc_codarticulo]
			      ,[int_cantidad]
			      ,[flt_preciosugerido]
			      ,[flt_precioentregado]
			      ,[flt_igv]) VALUES (${detallePedido.codCompania}, ${detallePedido.codPedido}, ${detallePedido.smiOrden}, ${detallePedido.codArticulo}, ${detallePedido.cantidad}, ${detallePedido.precioSugerido}, ${detallePedido.precioEntregado}, ${detallePedido.igv})
			""")
			results = sql.execute("Set IDENTITY_INSERT DETALLE_PEDIDO off")

			session.clear()
			return true
		}catch(Exception | Error e){
			println 'Error en el almacenamiento de DetallePedidoEssa: '+e
			return false
		}

	}
}
