package com.seratic.essa.extractores

import java.util.Date;

import grails.transaction.Transactional

import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

class AgendaExtractorService {

	static transactional = false
	def agendaSqlService
	def sessionFactory_intermedio

	def extraer() {

		def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()
		def listaAgenda = agendaSqlService.consultaAgendasDesactualzadas()
		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		listaAgenda.eachWithIndex { agenda, index ->
			def clienteIntermedio = ClienteIntermedio.findByCodigo(agenda.id)?:new ClienteIntermedio()
			clienteIntermedio.codigo = agenda.id
			clienteIntermedio.nombre = agenda.agendaNombre
			clienteIntermedio.tipoDocumento = 'RUC'
			clienteIntermedio.numeroDocumento = agenda.ruc
			clienteIntermedio.fechaExtraccion = new Date()
			clienteIntermedio.fechaModificacionEssa = agenda.tpTelefonica
			clienteIntermedio.estadoEnvio = false
			clienteIntermedio.moneda = agenda.idMonedaVta
			clienteIntermedio.fechaIngreso = agenda.fechaIngreso
			clienteIntermedio.direccion = agenda.direccion
			clienteIntermedio.falloEnvio = false


			if(clienteIntermedio.save(flush:true)){

				def rutaIntermedio = RutaIntermedio.findByIdAgenda(agenda.id)?:new RutaIntermedio()
				rutaIntermedio.idAgenda = agenda.id
				rutaIntermedio.idVendedor = agenda.idVendedor
				rutaIntermedio.fechaExtraccion = new Date()
				rutaIntermedio.fechaModificacionEssa = agenda.tpTelefonica
				rutaIntermedio.estadoEnvio = false
				rutaIntermedio.nombreVendedor = agenda.nombreVendedor
				rutaIntermedio.falloEnvio = false

				if(rutaIntermedio.save(flush:true)){
					listaIdRegistrados.add(agenda.id)
				}else{
					println 'Se presento un inconveniente al tratar de guardar Ruta Intermedio Asociada a la Agenda: '+agenda.id
					println rutaIntermedio.errors
				}
				
				if(index.mod(100)==0) {
					cleanUpGorm()
				}

			}else{
				println 'Se presento un inconveniente al tratar de guardar Cliente Intermedio Asociada a la Agenda: '+agenda.id
				println clienteIntermedio.errors
			}

		}

		agendaSqlService.actualizaEstado(listaIdRegistrados)

		println "-----FINALIZA REGISTRO: "+new Date()

	}

	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
