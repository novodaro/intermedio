package com.seratic.essa.extractores

import grails.transaction.Transactional
import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*
import com.seratic.essa.utils.*


class CtaCteDocExtractorService {

	static transactional = false
	def ctaCteDocSqlService
	def sessionFactory_intermedio

	def extraer() {
		def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()

		def listaCtaCteDoc = ctaCteDocSqlService.consultaAgendasDesactualzadas()

		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		listaCtaCteDoc.eachWithIndex { ctaCteDoc, index ->
			def clienteIntermedio = ClienteIntermedio.findByCodigo(ctaCteDoc.idAgenda)
			if(clienteIntermedio){
				clienteIntermedio.estadoEnvio = false
				clienteIntermedio.falloEnvio = false

				if(clienteIntermedio.save(flush:true)){
					def listaId = Utilidad.clobToString(ctaCteDoc.listaID_Doc)

					listaIdRegistrados.addAll(listaId)
					if(index.mod(100)==0) {
						cleanUpGorm()
					}
				}else{
					println 'Ocurrio un inconveniente al actualizar en el intermedio el cliente: ' + ctaCteDoc.idAgenda
					println clienteIntermedio.errors
				}
			}else{
				println 'No se ha podido actualizar el siguiente ClienteIntermedio dado que no existe: '+ctaCteDoc.idAgenda
			}

		}
		if(listaIdRegistrados)
			ctaCteDocSqlService.actualizaEstado(listaIdRegistrados)
		println "-----FINALIZA REGISTRO: "+new Date()
	}

	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
