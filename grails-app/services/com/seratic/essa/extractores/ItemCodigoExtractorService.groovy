package com.seratic.essa.extractores

import grails.transaction.Transactional

import com.seratic.essa.dbEssa.*
import com.seratic.essa.dbIntermedio.*

import grails.util.Holders

class ItemCodigoExtractorService {

	static transactional = false
	def itemCodigoSqlService
	def sessionFactory_intermedio

	def extraer() {

		def grailsApplication = Holders.getGrailsApplication()
		def igv = grailsApplication.config.igv

		def listaIdRegistrados = []

		println "-----INICIA CONSULTA: "+new Date()
		def listaItemCodigo = ItemCodigoEssa.findAll(){
			tpRegistro in ['Actualizado', 'Insertado']
		}
		println "-----Finaliza CONSULTA: "+new Date()

		println "-----INICIA REGISTRO: "+new Date()

		def categoria1, categoria2, categoria3

		def codCategoria1, codCategoria2, codCategoria3
		def nomCategoria1, nomCategoria2, nomCategoria3

		listaItemCodigo.eachWithIndex { itemCodigo, index ->

			def productoIntermedio = ProductoIntermedio.findByCodigo(itemCodigo.idItem)?:new ProductoIntermedio()

			categoria1 = itemCodigo.idClas1
			categoria2 = itemCodigo.idClas2
			categoria3 = itemCodigo.idClas3

			codCategoria1 = '_C1'+(categoria1?categoria1.id:'')
			nomCategoria1 = categoria1?categoria1.clasificador:'SIN CATEGORÍA_1'

			codCategoria2 = codCategoria1+'_C2'+(categoria2?categoria2.id:'')
			nomCategoria2 = categoria2?categoria2.clasificador:'SIN CATEGORÍA_2'

			codCategoria3 = codCategoria2+'_C3'+(categoria3?categoria3.id:'')
			nomCategoria3 = categoria3?categoria3.clasificador:'SIN CATEGORÍA_3'

			productoIntermedio.codigoCategoria1 = codCategoria1
			productoIntermedio.nombreCategoria1 = nomCategoria1
			productoIntermedio.codigoCategoria2 = codCategoria2
			productoIntermedio.nombreCategoria2 = nomCategoria2
			productoIntermedio.codigoCategoria3 = codCategoria3
			productoIntermedio.nombreCategoria3 = nomCategoria3

			productoIntermedio.codigoSucursal = 1
			productoIntermedio.codigoUnidad = (itemCodigo.unidad.equals('NULL')||itemCodigo.unidad.equals('')||itemCodigo.unidad.equals(' ')||itemCodigo.unidad.equals(null))?'UND':itemCodigo.unidad
			productoIntermedio.codigo = itemCodigo.idItem
			productoIntermedio.habilitado = itemCodigo.activo?1:0
			productoIntermedio.igv = igv
			productoIntermedio.nombre = itemCodigo.descripcion
			productoIntermedio.precio = itemCodigo.precio
			productoIntermedio.peso = ''
			productoIntermedio.fechaExtraccion = new Date()
			productoIntermedio.fechaModificacionEssa = itemCodigo.tpTelefonica
			productoIntermedio.estadoEnvio = false
			productoIntermedio.falloEnvio = false

			if(productoIntermedio.save(flush:true)){
				listaIdRegistrados.add(itemCodigo.id)
				if(index.mod(100)==0) {
					cleanUpGorm()
				}
			}else{
				println 'Ocurrio un inconveniente al tratar de guardar en el intermedio el item codigo: ' + itemCodigo.id
				println productoIntermedio.errors
			}


		}

		if(listaIdRegistrados)
			itemCodigoSqlService.actualizaEstado(listaIdRegistrados)
			
		println "-----FINALIZA REGISTRO: "+new Date()

	}

	def cleanUpGorm(){
		def session = sessionFactory_intermedio.currentSession
		session.flush()
		session.clear()
	}
}
