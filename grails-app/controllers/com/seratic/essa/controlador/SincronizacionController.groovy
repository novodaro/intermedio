package com.seratic.essa.controlador

class SincronizacionController {

	def agendaExtractorService
	def ctaCteDocExtractorService
	def agendaLimiteCreditoExtractorService
	def almacenExtractorService
	def vendedorExtractorService
	def itemCodigoExtractorService
	def stockRealExtractorService
	def direccionExtractorService
	def clienteEnviadorService
	def rutaEnviadorService
	def productoEnviadorService
	def almacenEnviadorService
	def vendedorEnviadorService
	def pedidoDescargadorService
	def pedidoAlmacenadorService
	def stockVirtualCalculadorService
	def stockEnviadorService
	def pedidoLimpiadorService

	def index() {

		println '**************INICIO DECARGA PEDIDOS***************'
		pedidoDescargadorService.descargar()
		println '**************FIN DECARGA PEDIDOS***************'
		println '**************INICIO ALMACENA PEDIDOS***************'
		pedidoAlmacenadorService.almacenar()
		println '**************FIN ALMACENA PEDIDOS***************'

		println '**************INICIO STOCKREAL ***************'
		stockRealExtractorService.extraer()
		println '*************FIN STOCKREAL ***************'
		println '**************INICIO CALCULO STOCK_VIRTUAL ***************'
		stockVirtualCalculadorService.calcularStockVirtual()
		println '**************FIN CALCULO STOCK_VIRTUAL ***************'
		println '**************INICIO ENVIO VIRTUAL ***************'
		stockEnviadorService.cargaMultiple()
		println '**************FIN ENVIO VIRTUAL ***************'
		
		render(contentType: 'text/json') {[
			"success": true
		]}
	}


	def cargaManual(){
		println '******************INICIO EXTRACTORES******************'

		println '**************INICIO DIRECCIONES ***************'
		//direccionExtractorService.extraer()
		println '*************FIN DIRECCIONES ***************'

		println '**************INICIO AGENDA ***************'
		//agendaExtractorService.extraer()
		println '*************FIN AGENDA ***************'

		println '**************INICIO CTACTEDOC ***************'
		//ctaCteDocExtractorService.extraer()
		println '*************FIN CTACTEDOC ***************'

		println '**************INICIO LIMITECREDITO ***************'
		//agendaLimiteCreditoExtractorService.extraer()
		println '*************FIN LIMITECREDITO ***************'

		println '**************INICIO ALMACEN ***************'
		almacenExtractorService.extraer()
		println '*************FIN ALMACEN ***************'

		println '**************INICIO VENDEDOR ***************'
		//vendedorExtractorService.extraer()
		println '*************FIN VENDEDOR ***************'

		println '**************INICIO ITEMCODIGO ***************'
		//itemCodigoExtractorService.extraer()
		println '*************FIN ITEMCODIGO ***************'

		println '**************INICIO STOCKREAL ***************'
		//stockRealExtractorService.extraer()
		println '*************FIN STOCKREAL ***************'

		println '******************FIN EXTRACTORES*******************'


		println '******************* INICIO ENVIADORES*******************'

		println '**************INICIO ENVIO ALMACENES ***************'
		println almacenEnviadorService.enviar()
		println '**************FIN ENVIO ALMACENES ***************'

		println '**************INICIO ENVIO VENDEDORES ***************'
		//println vendedorEnviadorService.enviar()
		println '**************FIN ENVIO VENDEDORES ***************'
		
		println '**************INICIO ENVIO PRODUCTOS ***************'
		//println productoEnviadorService.cargaMultiple(100)
		println '**************FIN ENVIO PRODUCTOS ***************'
		
		println '**************INICIO ENVIO CLIENTES ***************'
		//println clienteEnviadorService.cargaMultiple(100)
		println '**************FIN ENVIO CLIENTES ***************'

		println '**************INICIO ENVIO RUTAS ***************'
		//rutaEnviadorService.cargaMultiple(100)
		println '**************FIN ENVIO RUTAS ***************'
		
		println '**************INICIO CALCULO STOCK_VIRTUAL ***************'
		//stockVirtualCalculadorService.calcularStockVirtual()
		println '**************FIN CALCULO STOCK_VIRTUAL ***************'
		println '**************INICIO ENVIO VIRTUAL ***************'
		//stockEnviadorService.cargaMultiple(100)
		println '**************INICIO ENVIO VIRTUAL ***************'


		println '******************* FIN ENVIADORES*******************'
	}
	
	def limpiarPedidos(){
		println '******************* INICIA LIMPIADOR*******************'
		pedidoLimpiadorService.limpiar()
		println '******************* FIN LIMPIADOR*******************'
	}
}
