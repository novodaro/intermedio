package com.seratic.essa.enviadores

import grails.transaction.Transactional
import com.seratic.essa.dbIntermedio.*
import grails.util.Holders
import wslite.soap.SOAPClient

class VendedorEnviadorService {

	static transactional = false
	def actualizaEstadosIntermedioService

	def enviar() {

		def listaVendedorIntermedio = VendedorIntermedio.findAll(){
			estadoEnvio == false
			falloEnvio == false
		}

		def grailsApplication = Holders.getGrailsApplication()
		def urlBase = grailsApplication.config.urlBase

		def respuesta = [statusCode:200, message:'', data: []]

		def listaEnviados =[]
		def listaErrores = []

		try{
			def listaUsuariosString = ""
			println "---carga Usuarios---"
			listaVendedorIntermedio.eachWithIndex { vendedorIntermedio, index ->

				try{
					def usuarioString = """
					<usuarios>
								<columnas>${vendedorIntermedio.id}</columnas><!--id incremental usuario: numérico enviado por intermedio-->             
								<columnas><![CDATA[${vendedorIntermedio.nombre}]]></columnas><!--nombre varchar  [vc_nomvendedor] -->
								<columnas><![CDATA[${vendedorIntermedio.codigo}]]></columnas><!--usuario login:varchar  [vc_nomvendedor]-->
								<columnas><![CDATA[${vendedorIntermedio.clave}]]></columnas><!--clave: varchar   [vc_clave]-->
					
								<!--POR DEFINIR CON LUIS-->
					
								<columnas>3</columnas><!--perfil. 3: movil. 2:web-->
								<columnas><![CDATA[${vendedorIntermedio.estado}]]></columnas><!--activo: bolean. x: true [ch_estado](por aclarar)-->
								<columnas><![CDATA[${vendedorIntermedio.campania}]]></columnas><!--campaña. Siempre 1-->
								<columnas><![CDATA[${vendedorIntermedio.codigoVendedor}]]></columnas><!--código usuario: varchar [vc_codvendedor]-->
								<columnas><![CDATA[${vendedorIntermedio.codigoAlmacen}]]></columnas><!--Almacen usuario-->
					</usuarios>
			"""

					listaEnviados.add(vendedorIntermedio.id)
					listaUsuariosString = listaUsuariosString+ usuarioString

				}catch(Exception| Error e){
					println 'Error presentado en el servicio de Vendedor Enviador: '+e.message
					listaErrores.add(vendedorIntermedio.id)
				}

			}

			def client = new SOAPClient(urlBase+'/GestorVisitas/WSCargaMaestros?wsdl')
			def response = client.send(
					"""<?xml version='1.0' encoding='UTF-8'?>
					<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://com.ws.gestorvisitas.enterprise.seratic.org/">
				   <soapenv:Header/>
				   <soapenv:Body>
				      <com:wmSincronizarUsuarios>
				         <!--Optional:-->
				         <usuariosRQT>
				            <!--Zero or more repetitions:-->
				            """
					+listaUsuariosString+
					"""
							</usuariosRQT>
					</com:wmSincronizarUsuarios>
					</soapenv:Body>
					</soapenv:Envelope>"""
					)


			respuesta.statusCode = response.httpResponse.statusCode

			response.wmSincronizarUsuariosResponse.return.resultados.errores.fila.each{e->
				listaErrores.add(listaEnviados[e.text().toInteger()-1])
			}

			respuesta.message=listaErrores.empty?"Todos los registros fueron actualizados":"Existieron registros que no se almacenaron correctamente"
			respuesta.data = listaErrores.unique()

			println respuesta

			if(!listaErrores.empty){
				println new Date()
				println listaErrores
				println listaUsuariosString
				println response.text
			}

		}catch(Error | Exception  e){
			println new Date()
			println "Error presentado en el Servicio de Usuarios "+ e
			respuesta.statusCode = 500
			respuesta.message = e.message

			listaErrores = listaEnviados

			println respuesta
		}


		def diferenciaId = listaEnviados - listaErrores

		if(diferenciaId)
			actualizaEstadosIntermedioService.actualizaEstadoVendedor(diferenciaId)

		if(listaErrores)
			actualizaEstadosIntermedioService.actualizaEstadoVendedorFallido(listaErrores)

		return respuesta
	}
}
