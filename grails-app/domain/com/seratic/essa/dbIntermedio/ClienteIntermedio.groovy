package com.seratic.essa.dbIntermedio

class ClienteIntermedio {

	String codigo
	String nombre
	String tipoDocumento
	String numeroDocumento
	String moneda
	Date fechaExtraccion
	Date fechaEnvio
	Date fechaModificacionEssa
	Boolean estadoEnvio 
	Date fechaIngreso
	String direccion
	Boolean falloEnvio	
	
    static constraints = {
		fechaEnvio nullable:true, blank:true
		fechaIngreso nullable:true, blank:true
		direccion nullable:true, blank:true
    }
	
	static mapping = {
		datasource 'intermedio'
		order "asc"
		sort "fechaModificacionEssa"
	}
}
