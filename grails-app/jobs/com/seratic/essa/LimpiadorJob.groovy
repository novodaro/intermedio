package com.seratic.essa



class LimpiadorJob {

	def pedidoLimpiadorService

	def concurrent = false
	
	static triggers = {
		//El cron del trigger se configura desde BD
	}

	def execute() {
		println '******************* INICIA LIMPIADOR*******************'
		pedidoLimpiadorService.limpiar()
		println '******************* FIN LIMPIADOR*******************'
	}
}
