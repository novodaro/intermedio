package com.seratic.essa



class MaestrosJob {
	def agendaExtractorService
	def ctaCteDocExtractorService
	def agendaLimiteCreditoExtractorService
	def almacenExtractorService
	def vendedorExtractorService
	def itemCodigoExtractorService
	def stockRealExtractorService
	def clienteEnviadorService
	def rutaEnviadorService
	def productoEnviadorService
	def almacenEnviadorService
	def vendedorEnviadorService
	def stockVirtualCalculadorService
	def stockEnviadorService

	def concurrent = false

	static triggers = {
		//El cron del trigger se configura desde BD
	}

	def execute() {
		println '******************INICIO EXTRACTORES******************'

		println '**************INICIO AGENDA ***************'
		agendaExtractorService.extraer()
		println '*************FIN AGENDA ***************'

		println '**************INICIO CTACTEDOC ***************'
		ctaCteDocExtractorService.extraer()
		println '*************FIN CTACTEDOC ***************'

		println '**************INICIO LIMITECREDITO ***************'
		agendaLimiteCreditoExtractorService.extraer()
		println '*************FIN LIMITECREDITO ***************'

		println '**************INICIO ALMACEN ***************'
		almacenExtractorService.extraer()
		println '*************FIN ALMACEN ***************'

		println '**************INICIO VENDEDOR ***************'
		vendedorExtractorService.extraer()
		println '*************FIN VENDEDOR ***************'

		println '**************INICIO ITEMCODIGO ***************'
		itemCodigoExtractorService.extraer()
		println '*************FIN ITEMCODIGO ***************'

		println '**************INICIO STOCKREAL ***************'
		stockRealExtractorService.extraer()
		println '*************FIN STOCKREAL ***************'

		println '******************FIN EXTRACTORES*******************'


		println '******************* INICIO ENVIADORES*******************'

		println '**************INICIO ENVIO ALMACENES ***************'
		println almacenEnviadorService.enviar()
		println '**************FIN ENVIO ALMACENES ***************'

		println '**************INICIO ENVIO VENDEDORES ***************'
		println vendedorEnviadorService.enviar()
		println '**************FIN ENVIO VENDEDORES ***************'

		println '**************INICIO ENVIO PRODUCTOS ***************'
		println productoEnviadorService.cargaMultiple(100)
		println '**************FIN ENVIO PRODUCTOS ***************'

		println '**************INICIO ENVIO CLIENTES ***************'
		println clienteEnviadorService.cargaMultiple(100)
		println '**************FIN ENVIO CLIENTES ***************'

		println '**************INICIO ENVIO RUTAS ***************'
		println rutaEnviadorService.cargaMultiple(100)
		println '**************FIN ENVIO RUTAS ***************'

		println '**************INICIO CALCULO STOCK_VIRTUAL ***************'
		stockVirtualCalculadorService.calcularStockVirtual()
		println '**************FIN CALCULO STOCK_VIRTUAL ***************'
		println '**************INICIO ENVIO VIRTUAL ***************'
		println stockEnviadorService.cargaMultiple(100)
		println '**************INICIO ENVIO VIRTUAL ***************'


		println '******************* FIN ENVIADORES*******************'
	}
}
