package com.seratic.essa.dbEssa

class AgendaLimiteCreditoEssa {

    String idAgenda
	Date emision
	Long estado
	Date vcmto
	Float importe
	Long accion
	String glosa
	Date tpTelefonica
	String tpRegistro

	static constraints = {
	}

	static mapping = {
		datasource 'essa'
		table 'AgendaLimiteCredito'
		version false
		order "asc"

		id column: 'ID', sqlType: 'numeric(18,0)'
		idAgenda column: 'ID_Agenda', sqlType: 'varchar(20)'
		emision column: 'Emision', sqlType: 'datetime'
		estado column: 'Estado', sqlType: 'numeric(18,0)'
		vcmto column: 'Vcmto', sqlType: 'datetime'
		importe column: 'Importe', sqlType: 'money'
		accion column: 'Accion', sqlType: 'numeric(18,0)'
		glosa column: 'Glosa', sqlType: 'varchar(200)'
		tpTelefonica column: 'TP_Telefonica', sqlType: 'datetime'
		tpRegistro column: 'TP_Registro', sqlType: 'varchar(20)'
	}
}
